# About the `Golearn` Project

This repository hosts sample code that I have written as part of my personal
journey learning to program in Go. For information on my learning path see
[Project Golearn](https://pirivan.gitlab.io/project/golearn/).
