// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package datautils

import (
	"fmt"
	"reflect"
	"testing"
)

// compareValues returns the result of comparing two objets, a and b. If the
// style parameter is false, the comparison is a <= b. If style is true, the
// comparison is a >= b.
func compareValues(a, b interface{}, style bool) bool {
	aVal := reflect.ValueOf(a)
	bVal := reflect.ValueOf(b)

	cmp := "<="
	if style {
		cmp = ">="
	}
	switch aVal.Kind() {
	case reflect.Int:
		if (!style && aVal.Int() <= bVal.Int()) ||
			(style && aVal.Int() >= bVal.Int()) {
			fmt.Println(a, cmp, b, "true")
			return true
		}
	case reflect.String:
		if (!style && aVal.String() <= bVal.String()) ||
			(style && aVal.String() >= bVal.String()) {
			fmt.Println(a, cmp, b, "true")
			return true
		}
	}
	fmt.Println(a, cmp, b, "false")
	return false
}

func Test_quicksort(t *testing.T) {
	type args struct {
		data []interface{}
		comp compare
	}
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		{"case 1", args{[]interface{}{}, compareValues}, []interface{}{}},
		{"case 2", args{[]interface{}{1}, compareValues}, []interface{}{1}},
		{"case 3", args{[]interface{}{3, 2, 5, 5, 10, 1}, compareValues}, []interface{}{1, 2, 3, 5, 5, 10}},
		{"case 3", args{[]interface{}{"m", "am", "al", "h", "p"}, compareValues}, []interface{}{"al", "am", "h", "m", "p"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := quicksort(tt.args.data, tt.args.comp); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("quicksort() = %v, want %v", got, tt.want)
			}
		})
	}
}
