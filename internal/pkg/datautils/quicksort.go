// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

// Package datautils implements useful operations on data structures
package datautils

// The fmt.Printf statements in this file do not print their output when running
// normal tests, i.e., when using the command 'make tests'. To see the output
// strings run the 'go test' command manually using the -v flag. For example:
// $ go test -v gitlab.com/pirivan/golearn/internal/pkg/datautils
import (
	"fmt"
)

// The compare type is a function that compares two objects, a and b.
// If the style parameter is false, the comparison is a <= b. If style is true,
// the comparison is a >= b.
type compare func(a, b interface{}, style bool) bool

// quicksort sorts an array of objects and returns the sorted array. The goal
// of this exercise is to use reflection to sort different types of objects, not
// to come up with a clever implementation of the quicksort algorithm.
// https://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting/Quicksort#Go
func quicksort(data []interface{}, fn compare) []interface{} {
	if len(data) < 2 {
		fmt.Printf("short %v\n", data)
		return data
	}

	pivot := data[0]
	l, r := 1, len(data)-1
	fmt.Println("start")
	for l <= r {
		fmt.Printf("in %v l=%d, r=%d\n", data, l, r)
		for l <= r && fn(data[l], pivot, false) {
			l++
		}
		for r >= l && fn(data[r], pivot, true) {
			r--
		}
		if l < r {
			data[l], data[r] = data[r], data[l]
		}
		fmt.Printf("out %v l=%d, r=%d\n", data, l, r)
	}

	if r > 0 {
		data[0], data[r] = data[r], data[0]
		quicksort(data[0:r], fn)
	}
	quicksort(data[l:], fn)

	fmt.Printf("final %v\n", data)
	return data
}
