// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package tcpserver

import (
	"bufio"
	"context"
	"io"
	"log"
	"sync"
)

// read captures client input
func (co *connIO) read(ctx context.Context) {
	reader := bufio.NewReader(co.conn)
L:
	for {
		select {
		case <-ctx.Done():
			break L
		default:
			buf, err := reader.ReadBytes('\n')
			if err != nil {
				ctrl := new(connIOErr)
				ctrl.reader = true
				ctrl.err = err
				co.errCh <- ctrl
			} else {
				co.dataIn <- buf
			}
		}
	}
}

// write sends data to the client
func (co *connIO) write(ctx context.Context) {
L:
	for {
		select {
		case <-ctx.Done():
			break L
		case data := <-co.dataOut:
			if _, err := co.conn.Write(data); err != nil {
				ctrl := new(connIOErr)
				ctrl.reader = false
				ctrl.err = err
				co.errCh <- ctrl
			}
		}
	}
}

// start initializes and runs the connection manager.
// there is one connection manager per client.
func (cm *connMgr) start(ctx context.Context) {
	key := ServerCtx("wg")
	servWG := ctx.Value(key).(*sync.WaitGroup)
	defer servWG.Done()

	// this connection's context
	connCtx, cancel := context.WithCancel(ctx)

	closeCM := func(msg string) {
		cancel()
		if err := cm.conn.Close(); err != nil {
			log.Printf("error closing connection %s", cm.id)
		}
		log.Print(msg)
	}

	notifyClose := func() {
		ctrl := new(connMgrSharedCtrl)
		ctrl.conn = cm.conn
		ctrl.enable = false
		*cm.channels.ctrlOut <- ctrl
	}

	// conn r/w
	rw := new(connIO)
	rw.conn = cm.conn
	rw.errCh = make(chan *connIOErr)
	rw.dataIn = make(chan []byte)
	rw.dataOut = make(chan []byte)

	// i/o
	go rw.read(connCtx)
	go rw.write(connCtx)

L:
	for {
		select {
		// incoming ctrl message from tcp server
		case ctrl := <-cm.channels.ctrlIn:
			if ctrl.enable {
				log.Printf("warning: enable connection ctrl for client %s", cm.id)
			} else {
				// server wants to close the connections
				closeCM("closing connection: " + cm.id)
				notifyClose()
				break L
			}

		// incoming error message from i/o
		case msg := <-rw.errCh:
			if msg.err != io.EOF {
				source := "read"
				if !msg.reader {
					source = "write"
				}
				log.Printf("%s %s error: %s", cm.id, source, msg.err)
			}
			closeCM("connection closed: " + cm.id)
			notifyClose()
			break L

		// incoming message from conn reader
		case data := <-rw.dataIn:
			msg := new(ClientData)
			msg.ID = cm.id
			msg.Data = data
			*cm.channels.connDataIn <- msg

		// outgoing message to conn writer
		case data := <-cm.channels.connDataOut:
			rw.dataOut <- data

		// program cancelled
		case <-ctx.Done():
			closeCM("closing connection: " + cm.id)
			break L
		}
	}
}
