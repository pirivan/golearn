// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package tcpserver

import (
	"context"
	"log"
	"net"
	"sync"
)

// control and data channels capacity
const connBuf = 128

// max number of connections
const maxConns = 2048

type serverState struct {
	appChs     *AppChannels
	listener   *net.TCPListener
	connMgrsCh chan *connMgrSharedCtrl
}

var state serverState

// Init initializes the TCPServer core strutures.
func (serv *TCPServer) Init() (*AppChannels, error) {
	server, err := net.ResolveTCPAddr("tcp4", serv.Address)
	if err != nil {
		return nil, err
	}

	listener, err := net.ListenTCP("tcp4", server)
	if err != nil {
		return nil, err
	}

	// create app channels
	appChs := new(AppChannels)
	appChs.CtrlIn = make(chan *ClientCtrl, connBuf)
	appChs.CtrlOut = make(chan *ClientCtrl, connBuf)
	appChs.DataIn = make(chan *ClientData, connBuf)
	appChs.DataOut = make(chan *ClientData, connBuf)

	// initialize the global state struct
	state.appChs = appChs
	state.listener = listener
	state.connMgrsCh = make(chan *connMgrSharedCtrl, connBuf)

	// done
	log.Printf("tcp server listening on address %s", listener.Addr().String())
	return state.appChs, nil
}

// Start gets the TCP server main loop going
func (serv *TCPServer) Start(ctx context.Context) {
	key := ServerCtx("wg")
	appWG := ctx.Value(key).(*sync.WaitGroup)
	defer appWG.Done()

	makeCMChannels := func() *connMgrChannels {
		chs := new(connMgrChannels)
		chs.ctrlIn = make(chan *connMgrCtrl, connBuf)
		chs.ctrlOut = &state.connMgrsCh
		chs.connDataIn = &state.appChs.DataIn
		chs.connDataOut = make(chan []byte, connBuf)
		return chs
	}

	// connMgr registry
	var conns map[*net.TCPConn]*connMgr
	var connIDs map[string]*connMgr

	// the connections context
	var wg sync.WaitGroup
	ctx0 := context.WithValue(context.Background(), key, &wg)
	connCtx, cancel := context.WithCancel(ctx0)

	// the connMgrs registries
	conns = make(map[*net.TCPConn]*connMgr, maxConns)
	connIDs = make(map[string]*connMgr, maxConns)

	// accept connections
	newConn := make(chan *net.TCPConn, connBuf)
	go accept(state.listener, &newConn)

L:
	for {
		select {
		// new connection
		case conn := <-newConn:
			// notify application
			cc := new(ClientCtrl)
			cc.ID = conn.RemoteAddr().String()
			cc.Enable = true
			state.appChs.CtrlIn <- cc

			// create a conn manager
			cm := new(connMgr)
			cm.id = cc.ID
			cm.conn = conn
			cm.channels = makeCMChannels()
			conns[conn] = cm
			connIDs[cm.id] = cm

			// start the conn manager
			log.Printf("new connection %s", cm.id)
			wg.Add(1)
			go cm.start(connCtx)

		// incoming app ctrl message
		case client := <-state.appChs.CtrlOut:
			id := client.ID
			if client.Enable {
				log.Printf("warning: enable app message for client %s", id)
			} else {
				// app wants to close this connection, notify connMgr
				ctrl := new(connMgrCtrl)
				ctrl.enable = false
				cm := connIDs[id]
				cm.channels.ctrlIn <- ctrl
			}

		// outgoing data from app
		case msg := <-state.appChs.DataOut:
			cm := connIDs[msg.ID]
			cm.channels.connDataOut <- msg.Data

		// incoming connMgr ctrl message
		case ctrl := <-state.connMgrsCh:
			id := conns[ctrl.conn].id
			if ctrl.enable {
				log.Printf("warning: enable ctrl message from client %s", id)
			} else {
				// connection closed, notify application
				cc := new(ClientCtrl)
				cc.ID = id
				cc.Enable = false
				state.appChs.CtrlIn <- cc
			}

		// program cancelled
		case <-ctx.Done():
			log.Print("tcp server is shutting down")
			cancel()
			wg.Wait()
			break L
		}
	}
}

// accept() is a forever loop to accept new connections
func accept(listener *net.TCPListener, ch *chan *net.TCPConn) {
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Fatalf("TCPServer connection accept error: %s", err)
		} else {
			*ch <- conn
		}
	}
}
