// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package tcpserver

import (
	"context"
	"net"
)

// ServerCtx is used to pass keys to context values
type ServerCtx string

// ClientCtrl conveys ctrl messages between the application and the tcp server
type ClientCtrl struct {
	ID     string
	Enable bool
}

// ClientData conveys data messages between the application and the tcp server
type ClientData struct {
	ID   string
	Data []byte
}

// AppChannels holds the set of channels between the application and the tcp
// server
type AppChannels struct {
	CtrlIn  chan *ClientCtrl
	CtrlOut chan *ClientCtrl
	DataIn  chan *ClientData
	DataOut chan *ClientData
}

// TCPServer core
type TCPServer struct {
	Address string
}

// ServerIface interface
type ServerIface interface {
	Init() (*AppChannels, error)
	Start(context.Context)
}

type connMgrSharedCtrl struct {
	conn   *net.TCPConn
	enable bool
}

type connMgrCtrl struct {
	enable bool
}

type connMgrChannels struct {
	ctrlIn      chan *connMgrCtrl
	ctrlOut     *chan *connMgrSharedCtrl
	connDataIn  *chan *ClientData
	connDataOut chan []byte
}

// connMgr core
type connMgr struct {
	id       string
	conn     *net.TCPConn
	channels *connMgrChannels
}

// connMgr interface
type connMgrIface interface {
	start(context.Context)
}

type connIOErr struct {
	reader bool
	err    error
}

type connIO struct {
	conn    *net.TCPConn
	errCh   chan *connIOErr
	dataIn  chan []byte
	dataOut chan []byte
}

// read/write (i/o) interface
type connIOIface interface {
	read(context.Context)
	write(context.Context)
}
