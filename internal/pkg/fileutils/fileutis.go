// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

// Package fileutils implements useful operations on files
package fileutils

import (
	"os"
)

// FileExists return true if a file with the provided name exists. It returns
// false otherwise.
func FileExists(name string) bool {
	if _, err := os.Stat(name); err == nil {
		return true
	}
	return false
}

// RemoveFile removes a file from the system
func RemoveFile(name string) error {
	return os.Remove(name)
}
