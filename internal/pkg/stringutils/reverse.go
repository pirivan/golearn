/*
This is free and unencumbered software released into the public domain.
See the LICENSE file for details

Source code from https://golang.org/doc/code.html
*/

// Package stringutils implements useful operations on strings
package stringutils

// Reverse accepts a string and returns another with the original content
// reversed.
func Reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}
