// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package stringutils

import (
	"io"
	"reflect"
	"strings"
	"testing"
)

func Test_scanReader(t *testing.T) {
	var data = strings.NewReader("one\ntwo\nthree")

	type args struct {
		input  io.Reader
		prompt string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"case 1", args{data, "prompt"}, []string{"one", "two", "three"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := scanReader(tt.args.input, tt.args.prompt); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("scanReader() = %v, want %v", got, tt.want)
			}
		})
	}
}
