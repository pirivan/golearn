// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

// Package stringutils implements useful operations on strings
package stringutils

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func displayPrompt(prompt string) {
	if prompt == "" {
		return
	}
	fmt.Print(prompt)
}

// isConsole returns true if stdin has not been redirected on the
// command line. Examples of redirection for stdin are:
//    $ cat - | commad
//    $ cat some-file.txt | command
//    $ command < some-file.txt
func isConsole() bool {
	fi, _ := os.Stdin.Stat()

	// ex: cat - | command
	// ex: cat some-file.txt | command
	var isPipe = fi.Mode()&os.ModeNamedPipe != 0

	// ex: command < some-file.txt
	var isRedirect = fi.Size() > 0

	return !(isPipe || isRedirect)
}

// scanReader reads multiline input from an io.Reader until EOF is detected. It
// returns the input as a array of strings. scanReader displays a prompt
// parameter on stdout if input is coming from the console.
func scanReader(reader io.Reader, prompt string) []string {
	var data []string

	scanner := bufio.NewScanner(reader)
	displayPrompt(prompt)
	for scanner.Scan() {
		data = append(data, scanner.Text())
		displayPrompt(prompt)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return data
}

// ReadFromStdin reads multiline input from stdin until EOF is detected. It
// returns the input as a array of strings. ReadFromStdin displays a prompt
// parameter on stdout if input is coming from the console.
func ReadFromStdin(prompt string) []string {
	if !isConsole() {
		prompt = ""
	}
	return scanReader(os.Stdin, prompt)
}

// ReadLineFromStdin reads and returns a single line captured from stdin
func ReadLineFromStdin(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	displayPrompt(prompt)
	text, _ := reader.ReadString('\n')
	return strings.TrimSuffix(text, "\n")
}
