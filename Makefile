#
# This is free and unencumbered software released into the public domain.
# See the LICENSE file for details.
# 

# Directory for binary files
# Default is GOBIN := $(GOPATH)/bin

# Directory for source code relative to current directory
# Default is CMDDIR := cmd

# Directory for internal libraries relative to current directory
# Default is INTERNALDIR := internal/pkg

include golang.mk
