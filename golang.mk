#
# This is free and unencumbered software released into the public domain.
# See the LICENSE file for details.
# 

#####################
# internal variables
#####################
# Default directory values
BINDIR := $(realpath $(GOBIN))
CMDDIR ?= cmd
INTERNALDIR ?= internal/pkg

# Current dir with respect to $GOPATH
SRCTREE := $(subst $(GOPATH)/src/,,$(shell pwd))

# Programms (commands) to build
ifeq ($(strip $(BINDIR)),)
	CMDTARGETS := $(shell go list -f "{{.Target}}" $(SRCTREE)/$(CMDDIR)/...)
else
	CMDTARGETS := $(shell export GOBIN=$(BINDIR); go list -f "{{.Target}}" $(SRCTREE)/$(CMDDIR)/...)
endif

# Internal libs to build
LIBDIRS := $(shell find $(INTERNALDIR) -type d | grep -v $(INTERNALDIR)$$)
GOLIBS := $(addprefix $(SRCTREE)/,$(LIBDIRS))
LIBTARGETS := $(shell go list -f "{{.Target}}" $(GOLIBS))

##############
# the targets
##############
.PHONY: all libs commands tests
all: libs commands

libs:
ifeq ($(strip $(BINDIR)),)
	go install -v $(SRCTREE)/$(INTERNALDIR)/...
else
	GOBIN=$(BINDIR); go install -v $(SRCTREE)/$(INTERNALDIR)/...
endif

commands:
ifeq ($(strip $(BINDIR)),)
	go install -v $(SRCTREE)/$(CMDDIR)/...
else
	export GOBIN=$(BINDIR); go install -v $(SRCTREE)/$(CMDDIR)/...
endif

tests:
	go test $(SRCTREE)/$(INTERNALDIR)/... $(SRCTREE)/$(CMDDIR)/... 

################
# other targets
################
.PHONY: clean cleanall
clean:
	@rm -f $(CMDTARGETS)
	@echo "commands clean"

cleanall: clean
	@rm -f $(LIBTARGETS)
	@echo "libs clean"
