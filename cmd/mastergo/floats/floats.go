// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Mastering Go, Chapter 1

	This program combines the three suggested exercises into a single one.
	Therefore this program:

	- Finds the sum, max/min, and average of all of its numeric command-line
	  arguments
	- Keeps reading numbers from stdin and updating stats until it gets the word
	  STOP as input
*/

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

// number of floats and non-floats in the input
var nFloats, nNaN int
var min, max, avg, sum float32

func updateStats(n float32) {
	if n < min {
		min = n
	}
	if n > max {
		max = n
	}
	avg = (float32(nFloats)*avg + n) / float32(nFloats+1)
	sum = +n
}

func processData(data string) {
	val, err := strconv.ParseFloat(data, 32)
	if err != nil {
		nNaN++
	} else {
		n := float32(val)
		if nFloats == 0 {
			min, max, avg, sum = n, n, n, n
		} else {
			updateStats(n)
		}
		nFloats++
	}
}

func printStats() {
	fmt.Println("floats: ", nFloats)
	fmt.Println("non-floats: ", nNaN)
	if nFloats > 0 {
		fmt.Println("min: ", min)
		fmt.Println("max: ", max)
		fmt.Println("sum: ", sum)
		fmt.Println("avg: ", avg)
	}
}

func main() {

	// process command-line arguments
	for i := 1; i < len(os.Args); i++ {
		processData(os.Args[i])
	}

	// process stdin
	f := os.Stdin
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		if text == "STOP" {
			printStats()
			os.Exit(0)
		} else {
			processData(text)
		}
	}
}
