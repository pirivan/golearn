package main

// #cgo CFLAGS: -I${SRCDIR}/clib
// #cgo LDFLAGS: ${SRCDIR}/clib/cenv.a
// #include <stddef.h>
// #include <stdlib.h>
// #include <cenv.h>
import "C"
import (
    "fmt"
    "unsafe"
)

func main() {
    cStr := C.get_uname()
    defer C.free(unsafe.Pointer(cStr.buff))

    buff := C.GoBytes(unsafe.Pointer(cStr.buff), cStr.size)
    fmt.Print(string(buff))
}
