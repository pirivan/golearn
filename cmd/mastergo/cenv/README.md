This GO program invokes the `get_uname()` method which is implemented
in C. The C source files are in the `clib` directory.

Yo must run the _make_ command in this diretory to create the C
library required by the `cenv.go` program.

You can run the command `make cenv` to generate the go binary locally
a well.
