#include <stdlib.h>
#include <stdio.h>
#include <sys/utsname.h>
#include "cenv.h"

cString_t cStr;

cString_t *get_uname() {
  struct utsname u;
  
  uname(&u);
  cStr.size = snprintf(NULL, 0, "%s release %s (%s) on %s\n",
                       u.sysname, u.release, u.version, u.machine) + 1;
  cStr.buff = (char *) malloc(cStr.size);
  snprintf(cStr.buff, cStr.size, "%s release %s (%s) on %s\n",
	   u.sysname, u.release, u.version, u.machine);
  return &cStr;
}
