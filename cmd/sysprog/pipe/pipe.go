// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 8

	This program gets two Go routines to communicate via system pipes. One pipe
	is used to send a "ping" message, and another to respond with a "pong"
	message. The program terminates when it receives the SIGTERM signal, which
	triggers the exchange of a "bye" message to end the interaction between the
	Go routines.

	$ bin/pipe
	2018/07/16 20:25:05 ping
	2018/07/16 20:25:05 pong
	2018/07/16 20:25:06 ping
	2018/07/16 20:25:06 pong
	2018/07/16 20:25:07 ping
	2018/07/16 20:25:07 pong
	^C2018/07/16 20:25:08 bye
	2018/07/16 20:25:08 bye!
	2018/07/16 20:25:08 Exiting after 3 ping/pong exchanges
*/

package main

import (
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type pipes struct {
	pingR, pingW *os.File
	pongR, pongW *os.File
	wg           sync.WaitGroup
}

type pingState struct {
	counter int
	eof     bool
}

// SIGTERM signal handler
func sigHandler(signals chan os.Signal, ps *pingState) {
	for {
		_ = <-signals
		ps.eof = true
	}
}

// the ping routine
func ping(pp *pipes, ps *pingState) {
	data := make([]byte, 4)
	msg := "ping"
	defer pp.wg.Done()

	for {
		if ps.eof {
			msg = "bye "
		}

		if _, err := pp.pingW.Write([]byte(msg)); err != nil {
			log.Print(err)
		}

		if _, err := pp.pongR.Read(data); err != nil {
			log.Print(err)
		}
		log.Printf("%s", string(data))

		if ps.eof {
			break
		}
		ps.counter++
		time.Sleep(1 * time.Second)
	}
}

// the pong routine
func pong(pp *pipes) {
	data := make([]byte, 4)
	msg := "pong"
	done := false
	defer pp.wg.Done()

	for {
		if done {
			break
		}
		if _, err := pp.pingR.Read(data); err != nil {
			log.Print(err)
		}
		log.Printf("%s", string(data))

		if string(data) == "bye " {
			msg = "bye!"
			done = true
		}
		if _, err := pp.pongW.Write([]byte(msg)); err != nil {
			log.Print(err)
		}
	}
}

func main() {
	var pp pipes
	var ps pingState
	var err error

	// signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	go sigHandler(sigs, &ps)

	// the pipes structure
	pp.pingR, pp.pingW, err = os.Pipe()
	if err != nil {
		log.Fatal(err)
	}
	pp.pongR, pp.pongW, err = os.Pipe()
	if err != nil {
		pp.pingR.Close()
		pp.pingW.Close()
		log.Fatal(err)
	}

	// the ping and pong routines
	pp.wg.Add(2)
	go ping(&pp, &ps)
	go pong(&pp)

	// the end
	pp.wg.Wait()
	pp.pingR.Close()
	pp.pingW.Close()
	pp.pongR.Close()
	pp.pongW.Close()
	log.Printf("Exiting after %d ping/pong exchanges\n", ps.counter)
}
