// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 8

	This program gets two Go routines to communicate via a Unix socket. One
	routine sends a "ping" message (the client) and the other responds with a
	"pong" message (the server). The exchange terminates when the program
	receives the SIGINT signal, which triggers the sending of a "bye" message
	to end the interaction between the Go routines.

	$ bin/socket
	2018/07/17 14:46:47 ping
	2018/07/17 14:46:47 pong

	2018/07/17 14:46:48 ping
	2018/07/17 14:46:48 pong

	2018/07/17 14:46:49 ping
	2018/07/17 14:46:49 pong

	^C2018/07/17 14:46:50 bye  <-- SIGINT sent here
	2018/07/17 14:46:50 bye!

	2018/07/17 14:46:50 Exiting after 3 ping/pong exchanges

	The program accepts additional connections when using the -c flag. They can
	be started from a different terminal as in the example below. Note that the
	program always waits for a final new connection after the ping routine ends.

	$ bin/socket -c
	2018/07/17 14:14:09 ping
	2018/07/17 14:14:09 pong
                               <--
	2018/07/17 14:14:10 ping   <-- Other connections can start here
	2018/07/17 14:14:10 pong   <--

	2018/07/17 14:14:11 ping
	2018/07/17 14:14:11 pong

	^C2018/07/17 14:14:13 bye  <-- SIGINT sent here
	2018/07/17 14:14:13 bye!   <-- ping routine ends here

	2018/07/17 14:14:34 one    <-- one final session starts here
	2018/07/17 14:14:35 two        on a different terminal
	2018/07/17 14:14:43 bye
	2018/07/17 14:14:43 Exiting after 3 ping/pong exchanges


	# an additional session on a different terminal
	$ socat - UNIX-CONNECT:/tmp/socket.sock
	one
	pong
	two
	pong
	bye
	bye!

	Note: The previous version of this file has a simpler implementation with no
	support for additional connections.
	commit 4f5d46de0bbd17ca53a31c11afe564bd3dda78f8
*/

package main

import (
	"flag"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type socket struct {
	file string
	conn bool
	so   net.Listener
	wg   sync.WaitGroup
}

type pingState struct {
	counter int
	eof     bool
}

// SIGTERM signal handler
func sigHandler(signals chan os.Signal, ps *pingState) {
	for {
		_ = <-signals
		ps.eof = true
	}
}

// the ping routine (client)
func client(ss *socket, ps *pingState) {
	data := make([]byte, 256)
	msg := "ping"

	c, err := net.Dial("unix", ss.file)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	defer ss.wg.Done()

	for {
		if ps.eof {
			msg = "bye"
		}

		if _, err = c.Write([]byte(msg)); err != nil {
			log.Fatal(err)
		}

		if n, err := c.Read(data); err != nil {
			log.Fatal(err)
		} else {
			log.Printf("%s\n", string(data[:n]))
		}

		if ps.eof {
			break
		}
		ps.counter++
		time.Sleep(1 * time.Second)
	}
}

// the pong routine (server)
func server(ss *socket, ps *pingState) {
	var wg sync.WaitGroup
	defer ss.wg.Done()

	// the server code
	pong := func(c net.Conn) {
		data := make([]byte, 256)
		msg := "pong\n"
		done := false
		defer c.Close()

		for {
			if done {
				wg.Done()
				break
			}
			if n, err := c.Read(data); err != nil {
				log.Fatal(err)
			} else {
				log.Printf("%s", string(data[:n]))
			}

			if string(data[:3]) == "bye" {
				msg = "bye!\n"
				done = true
			}

			if _, err := c.Write([]byte(msg)); err != nil {
				log.Fatal(err)
			}
		}
	}

	// accept connections
	accept := func() {
		c, err := ss.so.Accept()
		if err != nil {
			log.Fatal(err)
		}
		wg.Add(1)
		go pong(c)
	}

	// accept the connection from the ping routine
	accept()

	// optionally, accept additional connections
	// as long as the ping routine is alive
	for ss.conn && !ps.eof {
		accept()
	}

	// wait for all clients to finish
	wg.Wait()
}

func main() {
	var ps pingState
	var ss socket
	var err error
	flag.BoolVar(&ss.conn, "c", false, "accept additional connections")
	flag.Parse()

	// signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	go sigHandler(sigs, &ps)

	// create the socket file
	ss.file = "/tmp/socket.sock"
	syscall.Unlink(ss.file)
	ss.so, err = net.Listen("unix", ss.file)
	if err != nil {
		log.Fatal(err)
	}
	defer ss.so.Close()

	// the ping and pong routines
	ss.wg.Add(2)
	go server(&ss, &ps)
	go client(&ss, &ps)

	// the end
	ss.wg.Wait()
	log.Printf("Exiting after %d ping/pong exchanges\n", ps.counter)
}
