// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 3, ocurrencies.go

	This program reads a sequence of strings and displays their number of
	ocurrencies (repetitions). The -t command line argument can be used to limit
	the repetition report to those strings exceeding a threshold value.

	The program accepts input from the console or a stdin redirection:

	$ bin/occurrences
	String: 4
	String: e
	String: 4
	String: 6
	String: ^D
	Threshold is 0
	4 -> 2
	e -> 1
	6 -> 1

	$ echo "4 e 4 6" | bin/occurrences
	Threshold is 0
	4 -> 2
	e -> 1
	6 -> 1

	$ bin/occurrences < /tmp/test.txt
	Threshold is 0
	4 -> 2
	e -> 1
	6 -> 1
*/

package main

import (
	"flag"
	"fmt"
	"strings"

	"gitlab.com/pirivan/golearn/internal/pkg/stringutils"
)

// command line options
var threshold int

// init initializes the command line option variables
func init() {
	const (
		defaultThreshold = 0
		usage            = "count threshold, default is zero"
	)
	flag.IntVar(&threshold, "threshold", defaultThreshold, usage)
	flag.IntVar(&threshold, "t", defaultThreshold, usage+" (shorthand)")
}

func main() {

	flag.Parse()
	s := stringutils.ReadFromStdin("String: ")
	counts := make(map[string]int)

	for i := 0; i < len(s); i++ {
		data := strings.Fields(s[i])
		for _, word := range data {
			_, ok := counts[word]
			if ok {
				counts[word] = counts[word] + 1
			} else {
				counts[word] = 1
			}
		}
	}

	fmt.Printf("\nThreshold is %d\n", threshold)
	for key := range counts {
		if counts[key] > threshold {
			fmt.Printf("%s -> %d \n", key, counts[key])
		}
	}
}
