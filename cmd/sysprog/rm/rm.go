// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 5, rm.go

	This program deletes one or multiple files as specified by the -f command
	line argument.
*/

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	arguments := flag.String("f", "", "a comma-separated list of files to remove")
	verbose := flag.Bool("v", false, "explain what is being done")
	flag.Parse()

	if *verbose {
		fmt.Print("parsing parameters ... ")
	}
	if len(*arguments) == 0 {
		fmt.Println("Please provide a list of files to remove")
		os.Exit(1)
	}
	if *verbose {
		fmt.Println("done")
	}

	files := strings.Split(*arguments, ",")
	for _, file := range files {
		if *verbose {
			fmt.Printf("removing file %s ... ", file)
		}
		err := os.Remove(file)
		if err != nil {
			fmt.Println(err)
		} else {
			if *verbose {
				fmt.Println("done")
			}
		}
	}
}
