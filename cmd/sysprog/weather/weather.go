// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 11

	This program renders the current weather and forecast for a city.
	Cities must be stored in a database as the one created by the db
	program.

	Note: To run this program you need to have an OpenWeather APIkey and make it
	available as an environment variable first. OpenWeather keys are free and
	available from https://openweathermap.org/appid

	To start the program listening on port 9090 run the following commands:

	export OPENWEATHERMAP_API_KEY=123456789
	bin/weather -p 9090 cities.db

	Use the -text option to render the forecast in plain text form instead of
	an HTML table.

	The following end point are supported:

	* List all cities: GET http://localhost:9090/cities

	* Query current weather and forecast by city ID:
	  GET http://localhost:9090/forecast/6094817

*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

type jobType struct {
	dbname string
	apikey string
	text   bool
}

var job jobType

func homeHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		fmt.Fprintf(w, "Weather App v1.0\nServing database %s", job.dbname)
	} else {
		http.NotFound(w, r)
	}
}

func main() {
	portPtr := flag.Int("p", 8080, "Port to listen on")
	textPtr := flag.Bool("text", false, "Display forecast in text form")

	flag.Parse()
	flags := flag.Args()

	if len(flags) == 0 {
		log.Fatal("usage: weather -p <port> db_name\n")
	}
	job.dbname = flags[0]
	job.text = *textPtr
	port := ":" + strconv.Itoa(*portPtr)

	job.apikey = os.Getenv("OPENWEATHERMAP_API_KEY")
	if len(job.apikey) == 0 {
		log.Fatal("Environment variable OPENWEATHERMAP_API_KEY not available")
	}

	r := mux.NewRouter()
	r.HandleFunc("/", homeHandler)
	r.HandleFunc("/cities", citiesHandler)
	r.HandleFunc("/forecast/{id:[0-9]+}", forecastHandler)
	http.ListenAndServe(port, r)
}
