// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"text/tabwriter"

	"github.com/gorilla/mux"
)

// http://json2struct.mervine.net/
type weatherType struct {
	Base   string `json:"base"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Cod   int `json:"cod"`
	Coord struct {
		Lat float64 `json:"lat"`
		Lon float64 `json:"lon"`
	} `json:"coord"`
	Dt   int `json:"dt"`
	ID   int `json:"id"`
	Main struct {
		GrndLevel float64 `json:"grnd_level"`
		Humidity  int     `json:"humidity"`
		Pressure  float64 `json:"pressure"`
		SeaLevel  float64 `json:"sea_level"`
		Temp      float64 `json:"temp"`
		TempMax   float64 `json:"temp_max"`
		TempMin   float64 `json:"temp_min"`
	} `json:"main"`
	Name string `json:"name"`
	Sys  struct {
		Country string  `json:"country"`
		Message float64 `json:"message"`
		Sunrise int     `json:"sunrise"`
		Sunset  int     `json:"sunset"`
	} `json:"sys"`
	Weather []struct {
		Description string `json:"description"`
		Icon        string `json:"icon"`
		ID          int    `json:"id"`
		Main        string `json:"main"`
	} `json:"weather"`
	Wind struct {
		Deg   float64 `json:"deg"`
		Speed float64 `json:"speed"`
	} `json:"wind"`
}

type uviType struct {
	Date    int     `json:"date"`
	DateIso string  `json:"date_iso"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Value   float64 `json:"value"`
}

type forecastType struct {
	City struct {
		Coord struct {
			Lat float64 `json:"lat"`
			Lon float64 `json:"lon"`
		} `json:"coord"`
		Country string `json:"country"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"city"`
	Cnt  int    `json:"cnt"`
	Cod  string `json:"cod"`
	List []struct {
		Clouds struct {
			All int `json:"all"`
		} `json:"clouds"`
		Dt    int    `json:"dt"`
		DtTxt string `json:"dt_txt"`
		Main  struct {
			GrndLevel float64 `json:"grnd_level"`
			Humidity  int     `json:"humidity"`
			Pressure  float64 `json:"pressure"`
			SeaLevel  float64 `json:"sea_level"`
			Temp      float64 `json:"temp"`
			TempKf    float64 `json:"temp_kf"`
			TempMax   float64 `json:"temp_max"`
			TempMin   float64 `json:"temp_min"`
		} `json:"main"`
		Rain struct{} `json:"rain"`
		Sys  struct {
			Pod string `json:"pod"`
		} `json:"sys"`
		Weather []struct {
			Description string `json:"description"`
			Icon        string `json:"icon"`
			ID          int    `json:"id"`
			Main        string `json:"main"`
		} `json:"weather"`
		Wind struct {
			Deg   float64 `json:"deg"`
			Speed float64 `json:"speed"`
		} `json:"wind"`
	} `json:"list"`
	Message float64 `json:"message"`
}

type forecastDetailType struct {
	Description string
	Humidity    int
	Temp        float64
	TempMax     float64
	TempMin     float64
}

type reportType struct {
	status int
	City   struct {
		ID      int
		Name    string
		Country string
		Lon     float64
		Lat     float64
	}
	Current struct {
		Description string
		Humidity    int
		Temp        float64
		TempMax     float64
		TempMin     float64
	}
	UVI struct {
		DateIso string
		Value   float64
	}
	Forecast []struct {
		DtTxt  string
		Detail forecastDetailType
	}
}

const (
	weatherAPI  = "http://api.openweathermap.org/data/2.5/weather?id=%s&units=metric&appid=%s"
	uviAPI      = "http://api.openweathermap.org/data/2.5/uvi?lon=%f&lat=%f&appid=%s"
	forecastAPI = "http://api.openweathermap.org/data/2.5/forecast?id=%d&units=metric&appid=%s"
)

func getWeatherReport(report *reportType, id string) error {
	var weather weatherType

	str := fmt.Sprintf(weatherAPI, id, job.apikey)
	response, err := http.Get(str)
	if err != nil {
		log.Printf("getCurrentWeather error: %s", err)
		return err
	}

	buf, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("getCurrentWeather error: %s", err)
		return err
	}

	if err := json.Unmarshal(buf, &weather); err != nil {
		log.Printf("getCurrentWeather error: %s", err)
		return err
	}

	report.status = weather.Cod
	report.City.ID = weather.ID
	report.City.Name = weather.Name
	report.City.Country = weather.Sys.Country
	report.City.Lon = weather.Coord.Lon
	report.City.Lat = weather.Coord.Lat
	report.Current.Description = weather.Weather[0].Description
	report.Current.Humidity = weather.Main.Humidity
	report.Current.Temp = weather.Main.Temp
	report.Current.TempMax = weather.Main.TempMax
	report.Current.TempMin = weather.Main.TempMin

	return nil
}

func getUVIReport(report *reportType) error {
	var uvi uviType

	str := fmt.Sprintf(uviAPI, report.City.Lon, report.City.Lat, job.apikey)
	response, err := http.Get(str)
	if err != nil {
		log.Printf("getUVIReport error: %s", err)
		return err
	}

	buf, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("getUVIReport error: %s", err)
		return err
	}

	if err := json.Unmarshal(buf, &uvi); err != nil {
		log.Printf("getUVIReport error: %s", err)
		return err
	}

	report.UVI.DateIso = uvi.DateIso
	report.UVI.Value = uvi.Value

	return nil
}

func getForecastReport(report *reportType) error {
	var forecast forecastType

	str := fmt.Sprintf(forecastAPI, report.City.ID, job.apikey)
	response, err := http.Get(str)
	if err != nil {
		log.Printf("getForecastReport error: %s", err)
		return err
	}

	buf, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("getForecastReport error: %s", err)
		return err
	}

	if err := json.Unmarshal(buf, &forecast); err != nil {
		log.Printf("getForecastReport error: %s", err)
		return err
	}

	for _, fcast := range forecast.List {
		item := struct {
			DtTxt  string
			Detail forecastDetailType
		}{
			DtTxt: fcast.DtTxt,
			Detail: forecastDetailType{
				Description: fcast.Weather[0].Description,
				Humidity:    fcast.Main.Humidity,
				Temp:        fcast.Main.Temp,
				TempMax:     fcast.Main.TempMax,
				TempMin:     fcast.Main.TempMin,
			},
		}
		report.Forecast = append(report.Forecast, item)
	}
	return nil
}

func printHTMLReport(w http.ResponseWriter, report *reportType) {
	t, err := template.ParseFiles("cmd/sysprog/weather/forecast.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		fmt.Fprintln(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	if err = t.ExecuteTemplate(w, t.Name(), *report); err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		fmt.Fprintln(w, err)
	}
}

func printTxtReport(w http.ResponseWriter, report *reportType) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s, %s: %s\nTemp: %.f, Tmax: %.f, Tmin: %.f, Humidity: %d%%\n",
		report.City.Name, report.City.Country, report.Current.Description,
		report.Current.Temp, report.Current.TempMax, report.Current.TempMin, report.Current.Humidity)
	fmt.Fprintf(w, "UVI index %.2f (%s)\n", report.UVI.Value, report.UVI.DateIso)
	fmt.Fprintln(w, "Five Days Weather Forecast")

	tw := new(tabwriter.Writer)
	defer tw.Flush()
	tw.Init(w, 0, 8, 1, '\t', 0) // format tab-separated columns with a tab stop of 8.
	fmt.Fprintln(tw, "Date\tWeather\tTemp\tTempMax\tTempMin\tHumidity")
	date := ""
	for _, f := range report.Forecast {
		tmp := f.DtTxt[:10]
		if tmp != date {
			fmt.Fprintf(tw, "\t\t\t\t\t\n")
			date = tmp
		}
		fmt.Fprintf(tw, "%s\t%s\t%.2f\t%.2f\t%.2f\t%d\n",
			f.DtTxt, f.Detail.Description, f.Detail.Temp,
			f.Detail.TempMax, f.Detail.TempMin, f.Detail.Humidity)
	}
}

func forecastHandler(w http.ResponseWriter, r *http.Request) {
	var report reportType
	vars := mux.Vars(r)

	if err := getWeatherReport(&report, vars["id"]); err != nil {
		log.Fatalf("forecastHandler error: %s", err)
	}
	if report.status != http.StatusOK {
		w.WriteHeader(report.status)
		return
	}

	if err := getUVIReport(&report); err != nil {
		log.Fatalf("forecastHandler error: %s", err)
	}
	if report.status != http.StatusOK {
		w.WriteHeader(report.status)
		return
	}

	if err := getForecastReport(&report); err != nil {
		log.Fatalf("forecastHandler error: %s", err)
	}
	if report.status != http.StatusOK {
		w.WriteHeader(report.status)
		return
	}

	if job.text {
		printTxtReport(w, &report)
	} else {
		printHTMLReport(w, &report)
	}
}
