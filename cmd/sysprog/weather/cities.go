// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"encoding/json"
	"html/template"
	"net/http"
	"os/exec"
)

type cityType struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Country     string `json:"country"`
	Coordinates struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	} `json:"coord"`
}
type citiesType []cityType

var cities citiesType

var tmplCities = `<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Using Go HTML Templates</title>
	<style>
		html {
			font-size: 16px;
		}
		table, th, td {
		    border: 1px solid gray;
		}
	</style>
</head>
<body>

<h2 alight="center">Weather Cities</h2>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Country</th>
			<th>Longitude</th>
			<th>latitude</th>
		</tr>
	</thead>
	<tbody>
{{ range . }}
<tr>
	<td> {{ .ID }}</a> </td>
	<td> {{ .Name }} </td>
	<td> {{ .Country }} </td>
	<td> {{ .Coordinates.Lon }} </td>
	<td> {{ .Coordinates.Lat }} </td>
</tr>
{{ end }}
	</tbody>
</table>

</body>
</html>
`

func citiesHandler(w http.ResponseWriter, r *http.Request) {
	out, err := exec.Command("bin/db", "list", "-json", job.dbname).Output()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		if err := json.Unmarshal(out, &cities); err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
		} else {
			w.WriteHeader(http.StatusOK)
			t := template.New("cities")
			t = template.Must(t.Parse(tmplCities))
			t.Execute(w, cities)
		}
	}
}
