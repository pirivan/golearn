// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 7, insertLineNumber.go

	This program adds line numbers to a file by writing the changes to a
	temporary file first, and then copying over the result back to the original
	file.
*/

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	minusINIT := flag.Int("init", 1, "Initial Value")
	flag.Parse()
	flags := flag.Args()

	if len(flags) == 0 {
		fmt.Printf("usage: insertLineNumber <file1> [<file2> [... <fileN]]\n")
		os.Exit(1)
	}

	// create a temporary file in the system
	// we reuse the same temporary file for all input files
	tmpfile, err := ioutil.TempFile("", "golearn")
	if err != nil {
		log.Fatal(err)
	}

	lineNumber := *minusINIT
	for _, filename := range flags {
		fmt.Println("Processing:", filename)

		// reset temp file pointer so that copied lines start from the
		// beginning of the temporary file
		if _, err := tmpfile.Seek(0, 0); err != nil {
			log.Fatal(err)
		}

		// open input file for reading
		srcfile, err := os.Open(filename)
		if err != nil {
			log.Fatal(err)
		}

		// copy lines from input to temporary storage
		scanner := bufio.NewScanner(srcfile)
		for scanner.Scan() {
			str := fmt.Sprintf("%d: %s \n", lineNumber, scanner.Text())
			if _, err := tmpfile.WriteString(str); err != nil {
				log.Fatal(err)
			}
			lineNumber++
		}

		// check for scan errors
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		// close input file
		if err := srcfile.Close(); err != nil {
			log.Fatal(err)
		}

		// open the input file for writing
		dstfile, err := os.Create(filename)
		if err != nil {
			log.Fatal(err)
		}

		// reset temp file to beginning so that the io.Copy() operation below
		// copies content from the start of the file
		if _, err := tmpfile.Seek(0, 0); err != nil {
			log.Fatal(err)
		}

		// copy temporary file to input file
		if _, err := io.Copy(dstfile, tmpfile); err != nil {
			log.Fatal(err)
		}

		// flush content to media
		if err := dstfile.Sync(); err != nil {
			log.Fatal(err)
		}

		// close input file
		if err := dstfile.Close(); err != nil {
			log.Fatal(err)
		}
	}

	// close and remove the temporary file
	tmpfile.Close()
	os.Remove(tmpfile.Name())

	// done
	fmt.Println("Processed", lineNumber-*minusINIT, "lines!")
}
