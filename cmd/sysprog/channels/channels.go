// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 9

	This program gets two Go routines to communicate via channels. One channel
	is used to send a "ping" message, and another to respond with a "pong"
	message. The program terminates when it receives the SIGTERM signal, which
	triggers the exchange of a "bye" message to end the interaction between the
	Go routines.

	$ bin/channels
	2018/07/18 11:59:03 ping
	2018/07/18 11:59:03 pong
	2018/07/18 11:59:04 ping
	2018/07/18 11:59:04 pong
	2018/07/18 11:59:05 ping
	2018/07/18 11:59:05 pong
	^C2018/07/18 11:59:06 bye
	2018/07/18 11:59:06 bye!
	2018/07/18 11:59:06 Exiting after 3 ping/pong exchanges
*/

package main

import (
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type channels struct {
	pingCh, pongCh chan string
	wg             sync.WaitGroup
}

type pingState struct {
	counter int
	eof     bool
}

// SIGTERM signal handler
func sigHandler(signals chan os.Signal, ps *pingState) {
	for {
		_ = <-signals
		ps.eof = true
	}
}

// the ping routine
func ping(cc *channels, ps *pingState) {
	msg := "ping"
	defer cc.wg.Done()

	for {
		if ps.eof {
			msg = "bye"
		}

		cc.pingCh <- msg
		data, _ := <-cc.pongCh
		log.Printf("%s", data)

		if ps.eof {
			break
		}
		ps.counter++
		time.Sleep(1 * time.Second)
	}
}

// the pong routine
func pong(cc *channels) {
	msg := "pong"
	done := false
	defer cc.wg.Done()

	for {
		if done {
			break
		}

		data, _ := <-cc.pingCh
		log.Printf("%s", data)

		if data == "bye" {
			msg = "bye!"
			done = true
		}
		cc.pongCh <- msg
	}
}

func main() {
	var cc channels
	var ps pingState

	// signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	go sigHandler(sigs, &ps)

	// the channels structure
	cc.pingCh = make(chan string)
	cc.pongCh = make(chan string)

	// the ping and pong routines
	cc.wg.Add(2)
	go ping(&cc, &ps)
	go pong(&cc)

	// the end
	cc.wg.Wait()
	close(cc.pingCh)
	close(cc.pongCh)
	log.Printf("Exiting after %d ping/pong exchanges\n", ps.counter)
}
