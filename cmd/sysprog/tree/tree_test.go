package main

import (
	"reflect"
	"testing"
)

func Test_search(t *testing.T) {
	tree := create(30, 100)
	type args struct {
		t *Tree
		v int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"case 5", args{tree, 5}, false},
		{"case 15", args{tree, 15}, false},
		{"case 20", args{tree, 20}, false},
		{"case 22", args{tree, 22}, false},
		{"case 30", args{tree, 30}, false},
		{"case 0", args{tree, 0}, true},
		{"case 7", args{tree, 7}, true},
		{"case 13", args{tree, 13}, true},
		{"case 24", args{tree, 24}, true},
		{"case 29", args{tree, 29}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := search(tt.args.t, tt.args.v); got != tt.want {
				t.Errorf("search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_delete(t *testing.T) {
	createTree := func() *Tree {
		return create(10, 100)
		// [0 1 2 3 4 6 7 8 9]
		// The value of the root of the tree is 3
	}

	type args struct {
		t *Tree
		v int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"case nil", args{nil, 0}, "[]"},
		{"case 0", args{createTree(), 0}, "[1 2 3 4 6 7 8 9]"},
		{"case 1", args{createTree(), 1}, "[0 2 3 4 6 7 8 9]"},
		{"case 2", args{createTree(), 2}, "[0 1 3 4 6 7 8 9]"},
		{"case 3", args{createTree(), 3}, "[0 1 2 4 6 7 8 9]"},
		{"case 4", args{createTree(), 4}, "[0 1 2 3 6 7 8 9]"},
		{"case 5", args{createTree(), 5}, "[0 1 2 3 4 6 7 8 9]"},
		{"case 6", args{createTree(), 6}, "[0 1 2 3 4 7 8 9]"},
		{"case 7", args{createTree(), 7}, "[0 1 2 3 4 6 8 9]"},
		{"case 8", args{createTree(), 8}, "[0 1 2 3 4 6 7 9]"},
		{"case 9", args{createTree(), 9}, "[0 1 2 3 4 6 7 8]"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := encode(delete(tt.args.t, tt.args.v)); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("delete() = %v, want %v", got, tt.want)
			}
		})
	}
}
