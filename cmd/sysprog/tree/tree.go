// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 4, tree.go

	This program adds search and delete node capabilities to the binary
	tree program.
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Tree is a tree node
type Tree struct {
	Left  *Tree
	Value int
	Right *Tree
}

// traverse traverses a tree and returns an slice of its values
func traverse(t *Tree, items []int) []int {
	if t == nil {
		return items
	}
	items = traverse(t.Left, items)
	items = append(items, t.Value)
	items = traverse(t.Right, items)
	return items
}

func create(n int, seed int64) *Tree {
	var t *Tree
	if seed > 0 {
		rand.Seed(seed)
	} else {
		rand.Seed(time.Now().Unix())
	}
	for i := 0; i < 2*n; i++ {
		temp := rand.Intn(n)
		t = insert(t, temp)
	}
	return t
}

func insert(t *Tree, v int) *Tree {
	if t == nil {
		return &Tree{nil, v, nil}
	}
	if v == t.Value {
		return t
	}
	if v < t.Value {
		t.Left = insert(t.Left, v)
		return t
	}
	t.Right = insert(t.Right, v)
	return t
}

// search looks for a value v in a tree and returns true if it finds it.
// Otherwise it returns false.
func search(t *Tree, v int) bool {
	if t == nil {
		return false
	}
	if t.Value == v || search(t.Left, v) || search(t.Right, v) {
		return true
	}
	return false
}

// locate looks for a value v in a tree and returns a pair of node pointers,
// parent and target. If locate finds the value v in the tree, the target
// pointer points to the tree node containing v, and the parent pointer points
// to that tree node's parent. If locate does not find the value v, the target
// pointer is nil, and the parent pointer holds the original value used to
// invoke the function.
func locate(p *Tree, t *Tree, v int) (*Tree, *Tree) {
	if t == nil || v == t.Value {
		return p, t
	}

	var pp, tt *Tree
	pp, tt = locate(t, t.Left, v)
	if tt != nil {
		return pp, tt
	}
	pp, tt = locate(t, t.Right, v)
	if tt != nil {
		return pp, tt
	}
	return p, nil
}

// delete deletes a value from a tree, and reorganizes all nodes underneath the
// target node into a new tree, if required. It returns the modified original
// tree, or the original tree if the value to delete is not found.
func delete(t *Tree, v int) *Tree {
	// locate the target node containing the value to delete
	parent, target := locate(t, t, v)

	// value not found
	if target == nil {
		return t
	}

	// find all items (values) under the target node
	var items []int
	items = traverse(target.Left, items)
	items = traverse(target.Right, items)

	if len(items) == 0 {
		// the target node is a leaf node
		if parent.Left != nil && parent.Left.Value == v {
			parent.Left = nil
		} else {
			parent.Right = nil
		}
	} else {
		// build a new tree out of the items under the target node
		var newTree *Tree
		for _, v := range items {
			newTree = insert(newTree, v)
		}
		// update the target node with the new tree
		target.Value = newTree.Value
		target.Left = newTree.Left
		target.Right = newTree.Right
	}

	// return the updated tree
	return t
}

// encode returns a string representing the values stored in a tree.
func encode(t *Tree) string {
	var items []int
	items = traverse(t, items)
	return fmt.Sprint(items)
}

func main() {
	tree := create(10, 0)
	fmt.Println(encode(tree))
	fmt.Println("The value of the root of the tree is", tree.Value)
}
