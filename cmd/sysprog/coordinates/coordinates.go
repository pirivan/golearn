// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 2, interfaces.go

	The coordinates command prints the coordinates of three points defined in
	one, two, and three dimensions. The printing function accepts an interface
	type as a single parameter.
*/

package main

import (
	"fmt"
)

type coordinates interface {
	xaxis() int
	yaxis() int
	zaxis() int
}

type point1D struct {
	X int
}

type point2D struct {
	X point1D
	Y int
}

type point3D struct {
	P point2D
	Z int
}

func (s point1D) xaxis() int {
	return s.X
}

func (s point1D) yaxis() int {
	return 0
}

func (s point1D) zaxis() int {
	return 0
}

func (s point2D) xaxis() int {
	return s.X.xaxis()
}

func (s point2D) yaxis() int {
	return s.Y
}

func (s point2D) zaxis() int {
	return 0
}

func (s point3D) xaxis() int {
	return s.P.xaxis()
}

func (s point3D) yaxis() int {
	return s.P.yaxis()
}

func (s point3D) zaxis() int {
	return s.Z
}

func findCoordinates(a coordinates) string {
	return fmt.Sprintf("X: %d Y: %d Z: %d", a.xaxis(), a.yaxis(), a.zaxis())
}

func main() {

	x := point1D{X: -6}
	y := point2D{X: point1D{-1}, Y: 12}
	z := point3D{P: y, Z: 10}
	fmt.Println(findCoordinates(x))
	fmt.Println(findCoordinates(y))
	fmt.Println(findCoordinates(z))
}
