// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/* The coordinates command prints the coordinates of three points defined in
   one, two, and three dimensions. The function used for printing accepts an
   interface type as a single parameter.

Book: Go System Programming

Chapter: Writing Programs in Go

*/

package main

import "testing"

type testPoint struct {
	X int
	Y int
	Z int
}

func (s testPoint) xaxis() int {
	return s.X
}

func (s testPoint) yaxis() int {
	return s.Y
}

func (s testPoint) zaxis() int {
	return s.Z
}

func Test_findCoordinates(t *testing.T) {
	tp := testPoint{
		X: 1,
		Y: 10,
		Z: 100,
	}

	type args struct {
		a coordinates
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"case 1", args{tp}, "X: 1 Y: 10 Z: 100"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findCoordinates(tt.args.a); got != tt.want {
				t.Errorf("findCoordinates() = %v, want %v", got, tt.want)
			}
		})
	}
}
