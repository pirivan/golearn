// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 4, ddGo.go

	This program creates a new file of the specified size populated with random
	data. You specify the file size using the command line arguments `bs` and
	`count` for the the block size in bytes and the block count respectively.

	The program reports its progress everytime it receives the SIGUSR1 signal.
	In this implementation, the program sends the SIGUSR1 signal to itself
	everytime it writes a block of bytes.

	$ bin/dd -bs 1000 -count 5 /tmp/test
	Progress: 1000 of 5000 bytes
	Progress: 2000 of 5000 bytes
	Progress: 3000 of 5000 bytes
	Progress: 4000 of 5000 bytes
	Progress: 5000 of 5000 bytes

	$ ls -l /tmp/test
	-rw-rw-r-- 1 pedro pedro 5000 Jul 14 19:34 /tmp/test
*/

package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// proogram state
var bytesOut, fileSize int

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func createBytes(buf *[]byte, count int) {
	if count == 0 {
		return
	}
	for i := 0; i < count; i++ {
		intByte := byte(random(0, 9))
		*buf = append(*buf, intByte)
	}
}

func progressInfo(signal os.Signal) {
	fmt.Printf("Progress: %d of %d bytes\n", bytesOut, fileSize)
}

func main() {
	minusBS := flag.Int("bs", 0, "Block Size")
	minusCOUNT := flag.Int("count", 0, "Counter")
	flag.Parse()
	flags := flag.Args()

	if len(flags) == 0 {
		fmt.Println("Not enough arguments!")
		os.Exit(-1)
	}

	if *minusBS < 0 || *minusCOUNT < 0 {
		fmt.Println("Count or/and Byte Size < 0!")
		os.Exit(-1)
	}

	// signal handling
	fileSize = *minusBS * *minusCOUNT
	pid := syscall.Getpid()
	//this, _ := os.FindProcess(os.Getpid())
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGUSR1)
	go func() {
		for {
			sig := <-sigs
			progressInfo(sig)
		}
	}()

	filename := flags[0]
	rand.Seed(time.Now().Unix())

	_, err := os.Stat(filename)
	if err == nil {
		fmt.Printf("File %s already exists.\n", filename)
		os.Exit(1)
	}

	destination, err := os.Create(filename)
	if err != nil {
		fmt.Println("os.Create:", err)
		os.Exit(1)
	}

	buf := make([]byte, *minusBS)
	buf = nil
	for i := 0; i < *minusCOUNT; i++ {
		createBytes(&buf, *minusBS)
		if _, err := destination.Write(buf); err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}

		// update state
		bytesOut = (i + 1) * *minusBS

		// send signal to itself
		syscall.Kill(pid, syscall.SIGUSR1)
		//this.Signal(syscall.SIGUSR1)

		// give time for the signal to propagate and be processed
		time.Sleep(10 * time.Millisecond)
		buf = nil
	}
}
