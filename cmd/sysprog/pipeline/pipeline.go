// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 9

	This program calculates the total number of occurrences of a word in a set
	of text files. You enter the word and the names of the files using the
	command line arguments. The program additionally displays the number of
	lines, words, and characters on each file, and the corresponding accumulated
	totals.

	$ bin/pipeline -w VMWare ~/Documents/tmp/*.log
	2018/07/19 13:15:26 Searching for word "vmware" on 15 files
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-install.log: 1 (19, 141, 907)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.1.log: 14 (16, 74, 678)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.2.log: 14 (15, 70, 644)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.3.log: 14 (15, 70, 644)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.4.log: 14 (15, 70, 644)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.5.log: 14 (15, 70, 644)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.6.log: 14 (15, 70, 644)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.7.log: 14 (15, 70, 640)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.8.log: 14 (15, 70, 640)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.9.log: 14 (15, 70, 640)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-network.log: 14 (16, 74, 674)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-vmsvc.1.log: 1 (34, 375, 3069)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-vmsvc.2.log: 0 (41, 541, 4332)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-vmsvc.3.log: 1 (43, 478, 3951)
	2018/07/19 13:15:26 /home/pedro/Documents/tmp/vmware-vmsvc.log: 0 (24, 320, 2587)
	2018/07/19 13:15:26 Totals: 143 (313, 2563, 21338)
*/

package main

import (
	"bytes"
	"flag"
	"io/ioutil"
	"log"
	"strings"
)

type job struct {
	filename string
	word     string
	content  []byte
}

type result struct {
	job     *job
	matches int
	lines   int
	words   int
	chars   int
}

func jobSetUp(in <-chan *job, out chan<- *job) {
	for j := range in {
		input, err := ioutil.ReadFile(j.filename)
		if err != nil {
			log.Print(err)
		} else {
			j.content = input
			out <- j
		}
	}
	close(out)
}

func jobRun(in <-chan *job, out chan<- *result) {
	for j := range in {
		var r result
		r.job = j
		r.matches = strings.Count(string(j.content), j.word)
		r.lines = bytes.Count(j.content, []byte("\n"))
		r.words = len(bytes.Fields(j.content))
		r.chars = len(j.content)
		out <- &r
	}
	close(out)
}

func jobTotals(in <-chan *result) {
	var matches, lines, words, chars int
	for r := range in {
		matches += r.matches
		lines += r.lines
		words += r.words
		chars += r.chars
		log.Printf("%s: %d (%d, %d, %d)",
			r.job.filename, r.matches,
			r.lines, r.words, r.chars)
	}
	log.Printf("Totals: %d (%d, %d, %d)",
		matches, lines, words, chars)
}

func main() {
	word := flag.String("w", "", "Word to search for")

	// processes command-line arguments
	flag.Parse()
	if len(*word) == 0 {
		log.Fatal("Please specify a search word")
	}
	search := strings.ToLower(*word)

	jobSize := len(flag.Args())
	if jobSize == 0 {
		log.Fatal("Please provide a list of files to search")
	}
	log.Printf("Searching for word \"%s\" on %d files", search, jobSize)

	// the channels
	jobCh := make(chan *job, jobSize)
	runCh := make(chan *job, jobSize)
	resultsCh := make(chan *result, jobSize)

	// the job processing pipeline
	go jobSetUp(jobCh, runCh)
	go jobRun(runCh, resultsCh)

	// submit jobs
	for _, name := range flag.Args() {
		var j job
		j.filename = name
		j.word = search
		jobCh <- &j
	}
	close(jobCh)

	// search results
	jobTotals(resultsCh)
}
