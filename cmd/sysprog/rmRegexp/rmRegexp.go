// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 7, swapRE.go

	This program reads lines from a file a processes those lines formatted as in
	the following example (/var/log/syslog):

	Jul 12 00:14:34 ubuntu anacron[7268]: Job `cron.weekly' started

	The program removes the time portion of each matched line and displays the
	following:

	Jul 12 ubuntu anacron[7268]: Job `cron.weekly' started

	Unmatched lines are printed untouched.
*/

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
)

var re = regexp.MustCompile(`(.*) (\d\d:\d\d:\d\d) (.*)`)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Println("Please provide one log file to process!")
		os.Exit(-1)
	}
	numberOfLines := 0
	numberOfLinesMatched := 0

	filename := flag.Arg(0)
	f, err := os.Open(filename)
	if err != nil {
		fmt.Printf("error opening file %s", err)
		os.Exit(1)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		numberOfLines++
		line := scanner.Text()
		if re.MatchString(line) {
			numberOfLinesMatched++
			match := re.FindStringSubmatch(line)
			fmt.Println(match[1], match[3])
		} else {
			fmt.Println(line)
		}
	}

	fmt.Println("Line processed:", numberOfLines)
	fmt.Println("Line matched:", numberOfLinesMatched)
}
