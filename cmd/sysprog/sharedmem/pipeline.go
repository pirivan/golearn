// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 9

	This program is a variation of the pipeline.go program. In this version
	there are multiple jobRun() routines running in parallel instead of a single
	one processing jobs from the runCh channel, a channel that no longer exists.

	The jobRun() routines invoke the updateCounts() to update a set of running
	counters. Access to the running counters is done using a mutex to ensure
	consistent updates. The running counters keep track of the arithmetic mean
	for each of the lines, words, and chars values reported for each file.

	At the end of the program, jobTotals() computes the arithmetic means and
	compares them with the running counters. jobTotals() displays an error in
	case of discrepancies.

	Finally, this version of the program adds the -n flag to the command line.
	When used, it disables the use of the locking mutex when updating the
	running counters. This usually results in an error reported by jobTotals(),
	as shown in the following example.

	$ bin/sharedmem -n -w VMWare ~/Documents/tmp/*.log
	2018/07/21 17:27:15 Searching for word "vmware" on 15 files
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-install.log: 1 (19, 141, 907)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.8.log: 14 (15, 70, 640)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-vmsvc.1.log: 1 (34, 375, 3069)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.9.log: 14 (15, 70, 640)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-vmsvc.log: 0 (24, 320, 2587)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.5.log: 14 (15, 70, 644)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.log: 14 (16, 74, 674)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.1.log: 14 (16, 74, 678)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.4.log: 14 (15, 70, 644)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.3.log: 14 (15, 70, 644)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.2.log: 14 (15, 70, 644)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.7.log: 14 (15, 70, 640)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-vmsvc.2.log: 0 (41, 541, 4332)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-vmsvc.3.log: 1 (43, 478, 3951)
	2018/07/21 17:27:15 /home/pedro/Documents/tmp/vmware-network.6.log: 14 (15, 70, 644)
	2018/07/21 17:27:15 Totals: 143 (313, 2563, 21338)
	2018/07/21 17:27:15 Verification error on average counts 15(6):	20.87(19.00), 170.87(133.33), 1422.53(1095.67)
*/

package main

import (
	"bytes"
	"flag"
	"io/ioutil"
	"log"
	"strings"
	"sync"
)

type job struct {
	lock     bool
	filename string
	word     string
	content  []byte
}

type jobExec struct {
	wg  *sync.WaitGroup
	job *job
}

type result struct {
	job     *job
	matches int
	lines   int
	words   int
	chars   int
}

func jobSetUp(in <-chan *job, out chan<- *result) {
	var wg sync.WaitGroup
	for j := range in {
		input, err := ioutil.ReadFile(j.filename)
		if err != nil {
			log.Print(err)
		} else {
			var je jobExec
			j.content = input
			je.job = j
			je.wg = &wg
			wg.Add(1)
			go jobRun(&je, out)
		}
	}
	// wait for all processing routines to end
	wg.Wait()
	close(out)
}

func jobRun(je *jobExec, out chan<- *result) {
	var r result
	defer je.wg.Done()

	r.job = je.job
	r.matches = strings.Count(string(r.job.content), r.job.word)
	r.lines = bytes.Count(r.job.content, []byte("\n"))
	r.words = len(bytes.Fields(r.job.content))
	r.chars = len(r.job.content)
	updateCounts(&r, r.job.lock)
	out <- &r
}

func jobTotals(jobSize int, in <-chan *result) {
	var matches, lines, words, chars int
	for r := range in {
		matches += r.matches
		lines += r.lines
		words += r.words
		chars += r.chars
		log.Printf("%s: %d (%d, %d, %d)",
			r.job.filename, r.matches,
			r.lines, r.words, r.chars)
	}
	log.Printf("Totals: %d (%d, %d, %d)",
		matches, lines, words, chars)
	linesAvg := float32(lines) / float32(jobSize)
	wordsAvg := float32(words) / float32(jobSize)
	charsAvg := float32(chars) / float32(jobSize)
	if !verifyCounts(linesAvg, wordsAvg, charsAvg) {
		ra := getCounts()
		log.Fatalf("Verification error on average counts %d(%d): %.2f(%.2f), %.2f(%.2f), %.2f(%.2f)",
			jobSize, ra.files,
			linesAvg, ra.lines,
			wordsAvg, ra.words,
			charsAvg, ra.chars)
	}
}

func main() {
	word := flag.String("w", "", "Word to search for")
	lock := flag.Bool("n", false, "Don't use a locking mutex")

	// processes command-line arguments
	flag.Parse()
	if len(*word) == 0 {
		log.Fatal("Please specify a search word")
	}
	search := strings.ToLower(*word)

	jobSize := len(flag.Args())
	if jobSize == 0 {
		log.Fatal("Please provide a list of files to search")
	}
	log.Printf("Searching for word \"%s\" on %d files", search, jobSize)

	// the channels
	jobCh := make(chan *job, jobSize)
	resultsCh := make(chan *result, jobSize)

	// the job processing pipeline
	go jobSetUp(jobCh, resultsCh)

	// submit jobs
	for _, name := range flag.Args() {
		var j job
		j.lock = !*lock
		j.filename = name
		j.word = search
		jobCh <- &j
	}
	close(jobCh)

	// search results
	jobTotals(jobSize, resultsCh)
}
