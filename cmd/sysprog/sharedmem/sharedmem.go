// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	This module calculates the average number of lines, words, and characters
	per file. The average numbers are stored in global structure which with
	concurrent access protected by a Go mutex.
*/

package main

import (
	"math/rand"
	"sync"
	"time"
)

type runningCounts struct {
	files               int
	lines, words, chars float32
}

type counts struct {
	mutex  sync.RWMutex
	runAvg runningCounts
}

var avg counts

func getCounts() *runningCounts {
	var rc runningCounts

	rc.files = avg.runAvg.files
	rc.lines = avg.runAvg.lines
	rc.words = avg.runAvg.words
	rc.chars = avg.runAvg.chars
	return &rc
}

func setCounts(rc *runningCounts) {
	avg.runAvg.lines = rc.lines
	avg.runAvg.words = rc.words
	avg.runAvg.chars = rc.chars
	avg.runAvg.files = rc.files
}

func updateCounts(r *result, lock bool) {
	var rc *runningCounts

	// this function calculates the per-file running arithmetic average of lines, words,
	// and characters. It uses the following formula to update the averages:
	//
	// E_n = (x_1 + x_2 + ... + x_n) / n
	// E_(n+1) = (n * E_n + x_(n+1)) / (n+1)
	//
	// X: one of lines, words, or characters
	// n: the number of files that have been processed already, 0,1,2,...
	update := func() {
		rc = getCounts()
		rc.lines = float32(avg.runAvg.files)*avg.runAvg.lines + float32(r.lines)
		rc.words = float32(avg.runAvg.files)*avg.runAvg.words + float32(r.words)
		rc.chars = float32(avg.runAvg.files)*avg.runAvg.chars + float32(r.chars)
		rc.files++
		rc.lines /= float32(rc.files)
		rc.words /= float32(rc.files)
		rc.chars /= float32(rc.files)
		// simulate random processing time
		time.Sleep(time.Duration(rand.Intn(20)) * time.Microsecond)
		setCounts(rc)
	}

	if lock {
		avg.mutex.Lock()
		update()
		avg.mutex.Unlock()
	} else {
		update()
	}
}

func verifyCounts(lines, words, chars float32) bool {
	ra := getCounts()
	precision := float32(0.001)
	check := func(x, y float32) bool {
		if x-precision <= y && x+precision >= y {
			return true
		}
		return false
	}

	if check(ra.lines, lines) &&
		check(ra.words, words) &&
		check(ra.chars, chars) {
		return true
	}
	return false
}
