// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 2, functions.go

	This program prints the min and max values of an array of integers. It:

	- Captures numbers from stdin appending them as they are read.

	- Typical use of the minmax command uses stdin redirection. Examples are:

	$ bin/minmax
	$ cat - | bin/minmax
	$ cat some-file.txt | bin/minmax
	$ bin/minmax < some-file.txt

	- Accepts the -n option to enter a comma-separated string of numbers in the
	command line. Examples are:

	$ bin/minmax -n 1,2,3
	$ cat some-file.txt | bin/minmax -n 1,2,3

	Numbers entered using the -n option are included together with those
	coming from stdin.

	- Processing of the numbers stops when a non-integer is found in the input
	  stream.

	- Prints the (min, max) of the numbers, or (0,0) if no numbers are provided.
*/

package main

import (
	"flag"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/pirivan/golearn/internal/pkg/stringutils"
)

// command line options
var nCliOption string

// init initializes the command line option variables
func init() {
	const (
		defaultNCliOption = ""
		usageN            = "array of numbers separated by commas"
	)
	flag.StringVar(&nCliOption, "numbers", defaultNCliOption, usageN)
	flag.StringVar(&nCliOption, "n", defaultNCliOption, usageN+" (shorthand)")
}

// getStats returns the min and max values of an slice of ints.
// Returns 0, 0 if the slice is empty.
func getStats(numbers []int) (min, max int) {
	if len(numbers) == 0 {
		min, max = 0, 0
		return
	}
	sort.Ints(numbers)
	min = numbers[0]
	max = numbers[len(numbers)-1]
	return
}

// parseStringArray converts an array of strings into an array of integers,
// one integer per string item. Parsing stops if a non-integer is found.
func parseStringArray(numbers []int, data []string) []int {
	for _, s := range data {
		n, err := strconv.Atoi(s)
		if err == nil {
			numbers = append(numbers, n)
		} else {
			// non-integer found, stop processing numbers
			break
		}
	}
	return numbers
}

// readFromString reads integers from a comma-separated string of numbers.
// Parsing of numbers stops if a non-integer is found.
func readFromString(numbers []int, value string) []int {
	if len(value) == 0 {
		return numbers
	}

	for _, num := range strings.Split(value, ",") {
		n, err := strconv.Atoi(num)
		if err != nil {
			break
		}
		numbers = append(numbers, n)
	}
	return numbers
}

func main() {
	var numbers []int

	flag.Parse()
	s := stringutils.ReadFromStdin("Number: ")
	numbers = parseStringArray(numbers, s)
	numbers = readFromString(numbers, nCliOption)
	min, max := getStats(numbers)
	fmt.Printf("\n%v\n", numbers)
	fmt.Printf("(min %d, max %d)\n", min, max)
}

// BUG(ps) Godoc does not display function documentation in the main package.
//
// See https://github.com/golang/go/issues/5727
