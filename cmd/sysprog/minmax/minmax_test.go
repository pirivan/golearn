// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"reflect"
	"testing"
)

func Test_getStats(t *testing.T) {
	type args struct {
		in0 []int
	}
	tests := []struct {
		name    string
		args    args
		wantMin int
		wantMax int
	}{
		{"case 1", args{[]int{1, 3, -5, 7}}, -5, 7},
		{"case 2", args{[]int{0, 0}}, 0, 0},
		{"case 3", args{[]int{1, 1, 3, 3}}, 1, 3},
		{"case 4", args{[]int{}}, 0, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotMin, gotMax := getStats(tt.args.in0)
			if gotMin != tt.wantMin {
				t.Errorf("getStats() gotMin = %v, want %v", gotMin, tt.wantMin)
			}
			if gotMax != tt.wantMax {
				t.Errorf("getStats() gotMax = %v, want %v", gotMax, tt.wantMax)
			}
		})
	}
}

func Test_readFromString(t *testing.T) {
	type args struct {
		numbers []int
		value   string
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"case 1", args{[]int{}, ""}, []int{}},
		{"case 2", args{[]int{-4}, "1,2,3"}, []int{-4, 1, 2, 3}},
		{"case 3", args{[]int{1, 2, 3}, ""}, []int{1, 2, 3}},
		{"case 4", args{[]int{1, 2, 3}, "4,"}, []int{1, 2, 3, 4}},
		{"case 5", args{[]int{1, 3}, "3,6,a4"}, []int{1, 3, 3, 6}},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := readFromString(tt.args.numbers, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readFromString() = %v, want %v", got, tt.want)
			}
		})
	}
}
