// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

type cityType struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Country     string `json:"country"`
	Coordinates struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	} `json:"coord"`
}
type citiesType []cityType

var sqlCreateStatement = `
	create table cities (
		id integer primary key,
		name text,
		country text,
		longitude float,
		latitude float
	);
`

var sqlDeleteStatement = `
	delete from cities where id=?;
`

var sqlInsertStatement = `
	insert into cities (
		id,
		name,
		country,
		longitude,
		latitude
		) values(?,?,?,?,?);
`

var sqlQueryAllStatement = `
	select * from cities;
`

var sqlQueryStatement = `
	select * from cities where id=$1;
`

var sqlUpdateStatement = `
insert or replace into cities (
	id,
	name,
	country,
	longitude,
	latitude
	) values(?,?,?,?,?);
`
