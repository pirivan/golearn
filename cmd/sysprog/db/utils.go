// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
)

func openDB(name string) *sql.DB {
	db, err := sql.Open("sqlite3", name)
	if err != nil {
		log.Fatalf("Error opening database: %s", job.db)
	}
	return db
}

func closeDB(db *sql.DB) {
	db.Close()
}

func cityExists(db *sql.DB, id int) (bool, *cityType) {
	row := db.QueryRow(sqlQueryStatement, id)

	var city cityType
	err := row.Scan(&city.ID, &city.Name, &city.Country, &city.Coordinates.Lon, &city.Coordinates.Lat)
	if err != nil {
		return false, nil
	}
	return true, &city
}

func printCity(job *jobType, city *cityType) {
	if !job.json {
		fmt.Printf("id: %d\nname: %s\ncountry: %s\nLon: %.6f\nlat: %.6f\n",
			city.ID, city.Name, city.Country, city.Coordinates.Lon, city.Coordinates.Lat)
	} else {
		if jCity, err := json.Marshal(city); err != nil {
			log.Fatalf("print city error: %s", err)
		} else {
			fmt.Print(string(jCity))
		}
	}
}
