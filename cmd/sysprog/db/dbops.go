// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/pirivan/golearn/internal/pkg/stringutils"
)

func addCity(job *jobType) error {
	var city cityType
	err := errors.New("dummy")

	db := openDB(job.db)
	defer closeDB(db)

	exists, _ := cityExists(db, job.id)
	if exists {
		return fmt.Errorf("city %d exists already", job.id)
	}
	fmt.Printf("id: %d\n", job.id)

	city.ID = job.id
	city.Name = stringutils.ReadLineFromStdin("> name: ")
	city.Country = stringutils.ReadLineFromStdin("> country: ")
	for err != nil {
		city.Coordinates.Lon, err = strconv.ParseFloat(
			stringutils.ReadLineFromStdin("> longitude: "), 64)
	}
	err = errors.New("dummy")
	for err != nil {
		city.Coordinates.Lat, err = strconv.ParseFloat(
			stringutils.ReadLineFromStdin("> latitude: "), 64)
	}

	answer := stringutils.ReadLineFromStdin("Confirm (N/y): ")
	if "y" != strings.ToLower(answer) {
		return errors.New("operation aborted")
	}

	_, err = loadCity(db, &city)
	return err
}

func updateCity(job *jobType) error {
	err := errors.New("dummy")

	db := openDB(job.db)
	defer closeDB(db)

	exists, city := cityExists(db, job.id)
	if !exists {
		return fmt.Errorf("city %d does not exist", job.id)
	}
	fmt.Printf("id: %d\n", job.id)

	prompt := fmt.Sprintf("> name (%s): ", city.Name)
	answer := stringutils.ReadLineFromStdin(prompt)
	if len(answer) > 0 {
		city.Name = answer
	}

	prompt = fmt.Sprintf("> country (%s): ", city.Country)
	answer = stringutils.ReadLineFromStdin(prompt)
	if len(answer) > 0 {
		city.Country = answer
	}

	prompt = fmt.Sprintf("> longitude (%.6f): ", city.Coordinates.Lon)
	var f float64
	for err != nil {
		answer = stringutils.ReadLineFromStdin(prompt)
		if len(answer) == 0 {
			break
		}
		f, err = strconv.ParseFloat(answer, 64)
	}
	if len(answer) > 0 {
		city.Coordinates.Lon = f
	}

	err = errors.New("dummy")
	prompt = fmt.Sprintf("> latitude (%.6f): ", city.Coordinates.Lat)
	for err != nil {
		answer = stringutils.ReadLineFromStdin(prompt)
		if len(answer) == 0 {
			break
		}
		f, err = strconv.ParseFloat(answer, 64)
	}
	if len(answer) > 0 {
		city.Coordinates.Lat = f
	}

	answer = stringutils.ReadLineFromStdin("Confirm (N/y): ")
	if "y" != strings.ToLower(answer) {
		return errors.New("operation aborted")
	}

	_, err = refreshCity(db, city)
	return err
}

func deleteCity(job *jobType) error {
	db := openDB(job.db)
	defer closeDB(db)

	exists, city := cityExists(db, job.id)
	if !exists {
		return fmt.Errorf("city %d does not exist", job.id)
	}

	printCity(job, city)
	answer := stringutils.ReadLineFromStdin("Confirm (N/y): ")
	if "y" != strings.ToLower(answer) {
		return errors.New("operation aborted")
	}

	statement, err := db.Prepare(sqlDeleteStatement)
	if err != nil {
		return err
	}
	defer statement.Close()

	result, err := statement.Exec(job.id)
	if err != nil {
		return err
	}

	if n, _ := result.RowsAffected(); n != 1 {
		err = errors.New("invalid city id")
		return err
	}

	return nil
}
