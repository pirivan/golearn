// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"os"
)

func refreshCity(db *sql.DB, city *cityType) (*sql.Result, error) {
	statement, err := db.Prepare(sqlUpdateStatement)
	if err != nil {
		return nil, err
	}
	defer statement.Close()

	result, err := statement.Exec(city.ID,
		city.Name, city.Country, city.Coordinates.Lon, city.Coordinates.Lat)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func loadCity(db *sql.DB, city *cityType) (*sql.Result, error) {
	statement, err := db.Prepare(sqlInsertStatement)
	if err != nil {
		return nil, err
	}
	defer statement.Close()

	result, err := statement.Exec(city.ID,
		city.Name, city.Country, city.Coordinates.Lon, city.Coordinates.Lat)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func loadDB(db *sql.DB, job *jobType) (int, error) {
	var cities citiesType

	f, err := os.Open(job.data)
	if err != nil {
		return 0, err
	}

	jsonParser := json.NewDecoder(f)
	if err = jsonParser.Decode(&cities); err != nil {
		return 0, err
	}

	for n, city := range cities {
		if _, err := loadCity(db, &city); err != nil {
			return n, err
		}
	}
	return len(cities), nil
}

func newDB(job *jobType) int {
	db := openDB(job.db)
	defer closeDB(db)

	statement, err := db.Prepare(sqlCreateStatement)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := statement.Exec(); err != nil {
		log.Fatal(err)
	}

	n, err := loadDB(db, job)
	if err != nil {
		log.Fatal(err)
	}
	return n
}
