// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	This program manages a SQLite database of cities used by the weather.go program.
	The following are examples of valid commands:

	* Create a city database from a JSON file available from the OpenWeatherMap web
	  site: http://bulk.openweathermap.org/sample/city.list.min.json.gz
	  This is a sample JSON record:

	  [{"id":6078447,"name":"Morden","country":"CA","coord":{"lon":-98.101357,"lat":49.191898}}]

	  bin/db new -data city.list.min.json cities.db

	  Use the -f flag to allow overwriting the database in case it already exists.

	* List all city codes and names.

		bin/db list cities.db
		295582  ‘Azriqam                IL      34.700001       31.750000
		384848  Qarah Gawl al ‘Ulyā     IQ      45.632500       35.353889
		464176  Zavety Il’icha          RU      37.849998       56.049999
		464737  Zaponor’ye              RU      38.861942       55.639999

	  Use the -csv list option to list cities in CSV format.
	  Use the -json list option to list cities in JSON format.

	* List the cities for which weather forecast data is available in the
	  database already:

	  bin/db list -known cities.db

	* Add a city:

		bin/db add -id 6078447 cities.db
		id: 6078447
		> name: "Morden"
		> country: CA
		> longitude: -98.101357
		> latitude: 49.191898
		> confirm (N/y): y
		City 6078447 added

	* Update a city:

		bin/db update -id 6078447 cities.db
		id: 6078447
		> name: Morden
		> country: CA
		> longitude: -98.101357
		> latitude: 49.191898
		> confirm (N/y): y
		City 6078447 updated

	* Delete city:

		bin/db delete -id 6078447 cities.db
		id: 6078447
		name: Morden
		country: CA
		longitude: -98.101357
		latitude: 49.191898
		> confirm (N/y): y
		City 6078447 deleted

	* Query city:

		bin/db query -id 6078447 cities.db
		id: 6078447
		name: Morden
		country: CA
		longitude: -98.101357
		latitude: 49.191898

	* Report statistics about an existing database:

	  bin/db stats cities.db
*/

package main

import (
	"flag"
	"log"

	"gitlab.com/pirivan/golearn/internal/pkg/fileutils"
)

// the program's job description after parsing the CLI arguments
type jobType struct {
	db    string // db name
	name  string // job type (new|list|add|update|delete|stats)
	data  string // data for the "new" command
	force bool   // enable overwriting of the database
	csv   bool   // list cities in CSV format
	json  bool   // list cities in JSON format
	known bool   // known boolean for the "list" command
	id    int    // ID for the "add", "update", or "delete" commands
}

var job jobType

// init initializes the command line option variables
func init() {
	getJobName := func(fs *flag.FlagSet) string {
		l := fs.NArg()
		if l == 0 {
			log.Fatal("no database specified")
		} else if l > 1 {
			log.Fatal("too many database names")
		}
		return fs.Args()[0]
	}

	aud := func(fs *flag.FlagSet, id *int) {
		fs.Parse(flag.Args()[1:])
		if fs.NFlag() == 0 {
			log.Fatal("missing city id")
		}
		job.id = *id
		if job.id <= 0 {
			log.Fatalf("invalid city id %d", *id)
		}
		job.db = getJobName(fs)
	}

	// commands
	newCmd := flag.NewFlagSet("new", flag.ExitOnError)
	listCmd := flag.NewFlagSet("list", flag.ExitOnError)
	addCmd := flag.NewFlagSet("add", flag.ExitOnError)
	updateCmd := flag.NewFlagSet("update", flag.ExitOnError)
	deleteCmd := flag.NewFlagSet("delete", flag.ExitOnError)
	queryCmd := flag.NewFlagSet("query", flag.ExitOnError)

	// new command
	newDataPtr := newCmd.String("data", "", "Cities JSON data file")
	newForcePtr := newCmd.Bool("f", false, "Enable overwriting of the database")

	// list command
	listKnownPtr := listCmd.Bool("known", false, "List cities with forecast data")
	listCSVPtr := listCmd.Bool("csv", false, "List cities in CSV format")
	listJSONPtr := listCmd.Bool("json", false, "List cities in JSON format")

	// add command
	addIDPtr := addCmd.Int("id", 0, "Add a city with the specified ID")

	// update command
	updateIDPtr := updateCmd.Int("id", 0, "Update the city with the specified ID")

	// delete command
	deleteIDPtr := deleteCmd.Int("id", 0, "Delete the city with the specified ID")

	// query command
	queryIDPtr := queryCmd.Int("id", 0, "Query the city with the specified ID")
	queryJSONPtr := queryCmd.Bool("json", false, "List city in JSON format")

	flag.Parse()
	if len(flag.Args()) == 0 {
		log.Fatal("Please provide a database command")
	}
	job.name = flag.Args()[0]

	// find the command
	switch job.name {
	case "new":
		newCmd.Parse(flag.Args()[1:])
		if len(*newDataPtr) == 0 {
			log.Fatal("no city JSON data file provided")
		}
		job.data = *newDataPtr
		job.force = *newForcePtr
		job.db = getJobName(newCmd)
	case "list":
		listCmd.Parse(flag.Args()[1:])
		job.known = *listKnownPtr
		job.csv = *listCSVPtr
		job.json = *listJSONPtr
		if job.csv && job.json {
			log.Fatal("choose only one of csv or json formats")
		}
		job.db = getJobName(listCmd)
	case "add":
		aud(addCmd, addIDPtr)
	case "update":
		aud(updateCmd, updateIDPtr)
	case "delete":
		aud(deleteCmd, deleteIDPtr)
	case "query":
		aud(queryCmd, queryIDPtr)
		job.json = *queryJSONPtr
	case "stats":
		l := flag.NArg()
		if l == 1 {
			log.Fatal("no database specified")
		} else if l > 2 {
			log.Fatal("too many database names")
		}
		job.db = flag.Args()[1]
	default:
		log.Fatalf("\"%s\" is not a valid command", job.name)
	}
}

func main() {
	switch job.name {
	case "new":
		if fileutils.FileExists(job.db) {
			if !job.force {
				log.Fatalf("database exists, not overwriting: %s", job.db)
			} else {
				if err := fileutils.RemoveFile(job.db); err != nil {
					log.Fatalf("error removing database: %s", err)
				}
			}
		}
		if !fileutils.FileExists(job.data) {
			log.Fatalf("data file not found: %s", job.data)
		}
		n := newDB(&job)
		log.Printf("new database %s: %d cities", job.db, n)

	default:
		// DB file must exist
		if !fileutils.FileExists(job.db) {
			log.Fatalf("database not found: %s", job.db)
		}
		switch job.name {
		case "list":
			if err := listDB(&job); err != nil {
				log.Fatalf("list database error: %s", err)
			}
		case "add":
			if err := addCity(&job); err != nil {
				log.Fatalf("add city error: %s", err)
			}
			log.Printf("City %d added", job.id)
		case "update":
			if err := updateCity(&job); err != nil {
				log.Fatalf("update city error: %s", err)
			}
			log.Printf("City %d updated", job.id)
		case "delete":
			if err := deleteCity(&job); err != nil {
				log.Fatalf("delete city error: %s", err)
			}
			log.Printf("City %d deleted", job.id)
		case "query":
			if city, err := queryCity(&job); err != nil {
				log.Fatalf("query city error: %s", err)
			} else {
				printCity(&job, city)
			}
		case "stats":
			statsDB(&job)
		}
	}
}
