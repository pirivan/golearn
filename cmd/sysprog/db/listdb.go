// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.
package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"text/tabwriter"
)

func listDB(job *jobType) error {
	var city cityType

	csv := func(rows *sql.Rows) error {
		for rows.Next() {
			err := rows.Scan(&city.ID,
				&city.Name, &city.Country, &city.Coordinates.Lon, &city.Coordinates.Lat)
			if err != nil {
				return err
			}
			fmt.Printf("%d,%s,%s,%.6f,%.6f\n", city.ID,
				city.Name, city.Country, city.Coordinates.Lon, city.Coordinates.Lat)
		}
		return nil
	}

	json := func(rows *sql.Rows) error {
		var cities citiesType

		for rows.Next() {
			err := rows.Scan(&city.ID,
				&city.Name, &city.Country, &city.Coordinates.Lon, &city.Coordinates.Lat)
			if err != nil {
				return err
			}
			cities = append(cities, city)
		}

		if jCities, err := json.Marshal(cities); err != nil {
			return err
		} else {
			fmt.Print(string(jCities))
		}
		return nil
	}

	table := func(rows *sql.Rows) error {
		w := new(tabwriter.Writer)
		defer w.Flush()
		w.Init(os.Stdout, 0, 8, 0, '\t', 0) // format tab-separated columns with a tab stop of 8.
		for rows.Next() {
			err := rows.Scan(&city.ID,
				&city.Name, &city.Country, &city.Coordinates.Lon, &city.Coordinates.Lat)
			if err != nil {
				return err
			}
			fmt.Fprintf(w, "%d\t%s\t%s\t%.6f\t%.6f\n", city.ID,
				city.Name, city.Country, city.Coordinates.Lon, city.Coordinates.Lat)
		}
		return nil
	}

	db := openDB(job.db)
	defer closeDB(db)

	rows, err := db.Query(sqlQueryAllStatement)
	if err != nil {
		return err
	}
	defer rows.Close()

	if job.csv {
		return csv(rows)
	} else if job.json {
		return json(rows)
	}
	return table(rows)
}

func queryCity(job *jobType) (*cityType, error) {
	db := openDB(job.db)
	defer closeDB(db)

	exists, city := cityExists(db, job.id)
	if !exists {
		return nil, fmt.Errorf("city %d does not exist", job.id)
	}
	return city, nil
}

func statsDB(job *jobType) {
	log.Printf("Stats job %v", *job)
}
