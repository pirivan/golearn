// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 10

	This program is a variation of the socket.go program that now implements a
	socket accept routine with a timeout. The program terminates if no new
	socket connections are established within the specified timeout period.

	The -c command-line argument is no longer supported. Instead, use the -t
	argument to specify the connections timeout in seconds.

	$ bin/timeout -t 5
	2018/07/22 18:37:56 connection No. 1
	2018/07/22 18:37:56 (1) ping
	2018/07/22 18:37:56 (1) pong

	2018/07/22 18:37:57 (1) ping
	2018/07/22 18:37:57 (1) pong

	...

	2018/07/22 18:38:02 connection No. 2
	2018/07/22 18:38:03 (1) ping
	2018/07/22 18:38:03 (1) pong

	2018/07/22 18:38:03 (2) this
	2018/07/22 18:38:04 (1) ping
	2018/07/22 18:38:04 (1) pong

	2018/07/22 18:38:05 (1) ping
	2018/07/22 18:38:05 (1) pong

	2018/07/22 18:38:06 (2) works
	2018/07/22 18:38:07 (1) ping
	2018/07/22 18:38:07 (1) pong

	2018/07/22 18:38:08 (1) ping
	2018/07/22 18:38:08 (1) pong

	2018/07/22 18:38:08 (2) well
	2018/07/22 18:38:09 (1) ping
	2018/07/22 18:38:09 (1) pong

	...

	2018/07/22 18:38:10 (2) bye
	2018/07/22 18:38:11 (1) ping
	2018/07/22 18:38:11 (1) pong

	2018/07/22 18:38:12 connections timeout
	2018/07/22 18:38:13 (1) ping
	2018/07/22 18:38:13 (1) pong

	2018/07/22 18:38:14 (1) ping
	2018/07/22 18:38:14 (1) pong

	^C2018/07/22 18:38:15 (1) bye
	2018/07/22 18:38:15 (1) bye!

	2018/07/22 18:38:15 Exiting after 2 connections and 19 ping/pong exchanges
	2018/07/22 18:38:15 accept unix /tmp/socket.sock: use of closed network	connection

	$ socat - UNIX-CONNECT:/tmp/socket.sock
	this
	(2) pong
	works
	(2) pong
	well
	(2) pong
	bye
	(2) bye!
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type socket struct {
	file        string
	connections int
	so          net.Listener
	wg          sync.WaitGroup
}

type pingState struct {
	counter int
	eof     bool
}

type pongState struct {
	index int
	c     net.Conn
	wg    *sync.WaitGroup
}

// SIGTERM signal handler
func sigHandler(signals chan os.Signal, ps *pingState) {
	for {
		_ = <-signals
		ps.eof = true
	}
}

// the ping routine (client)
func ping(ss *socket, ps *pingState) {
	data := make([]byte, 256)
	msg := "ping"

	c, err := net.Dial("unix", ss.file)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	defer ss.wg.Done()

	for {
		if ps.eof {
			msg = "bye"
		}

		if _, err = c.Write([]byte(msg)); err != nil {
			log.Fatal(err)
		}

		if n, err := c.Read(data); err != nil {
			log.Fatal(err)
		} else {
			log.Printf("%s\n", string(data[:n]))
		}

		if ps.eof {
			break
		}
		ps.counter++
		time.Sleep(1 * time.Second)
	}
}

// the pong routine
func pong(ps *pongState) {
	defer ps.c.Close()
	defer ps.wg.Done()

	data := make([]byte, 256)
	msg := fmt.Sprintf("(%d) pong\n", ps.index)
	done := false

	for {
		if done {
			break
		}
		if n, err := ps.c.Read(data); err != nil {
			log.Printf("(%d) %s", ps.index, err)
			done = true
			break
		} else {
			log.Printf("(%d) %s", ps.index, string(data[:n]))
		}

		if string(data[:3]) == "bye" {
			msg = fmt.Sprintf("(%d) bye!\n", ps.index)
			done = true
		}

		if _, err := ps.c.Write([]byte(msg)); err != nil {
			log.Printf("(%d) %s", ps.index, err)
			done = true
			break
		}
	}
}

func acceptConnections(ss *socket, timeout time.Duration) {
	var wg sync.WaitGroup
	newConn := false
	connCh := make(chan net.Conn)
	ackCh := make(chan struct{})
	defer ss.wg.Done()

	// no more connections must be accepted if the ackCh channel is closed
	done := func() bool {
		if _, ok := <-ackCh; !ok {
			close(connCh)
			return true
		}
		return false
	}

	// accept socket connections.
	// new connections are sent via the connCh channel. An acknowledgement must
	// be received via the ackCh channel before the next Accpet() cycle can start.
	// The function exits when the ackCh channel is detected to be closed.
	go func() {
		for !done() {
			c, err := ss.so.Accept()
			if err != nil {
				log.Print(err)
				continue
			}
			connCh <- c
		}
	}()

	// signal the start of the accept loop above
	ackCh <- struct{}{}
L:
	for {
		select {
		// new connection
		case c := <-connCh:
			ackCh <- struct{}{}
			ss.connections++
			newConn = true
			log.Printf("connection No. %d", ss.connections)
			wg.Add(1)
			ps := pongState{index: ss.connections, c: c, wg: &wg}
			go pong(&ps)

		// new connections timeout
		case <-time.After(timeout):
			if newConn {
				newConn = false
			} else {
				log.Print("connections timeout")
				close(ackCh)
				break L
			}
		}
	}

	// wait for pong routines to finish
	wg.Wait()
}

func main() {
	var ps pingState
	var ss socket
	var timeout int
	var err error
	flag.IntVar(&timeout, "t", 1, "new connections timeout in seconds")
	flag.Parse()

	// signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	go sigHandler(sigs, &ps)

	// create the socket file
	ss.file = "/tmp/socket.sock"
	syscall.Unlink(ss.file)
	ss.so, err = net.Listen("unix", ss.file)
	if err != nil {
		log.Fatal(err)
	}
	defer ss.so.Close()

	// the ping and accept routines
	ss.wg.Add(2)
	go acceptConnections(&ss, time.Duration(timeout)*time.Second)
	go ping(&ss, &ps)

	// the end
	ss.wg.Wait()
	log.Printf("Exiting after %d connections and %d ping/pong exchanges\n",
		ss.connections, ps.counter)
}
