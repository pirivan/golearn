// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 5

	This program implements a minimum version of the `ls` command, with support
	for the -a and -l command line options.
*/

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strconv"
	"strings"
	"syscall"
)

// command line options
var (
	details bool
	all     bool
)

// init initializes the command line option variables
func init() {
	const (
		dDetails = false
		dAll     = false
		dDusage  = "use a long listing format"
		dAusage  = "do not ignore entries starting with ."
	)
	flag.BoolVar(&details, "l", dDetails, dDusage)
	flag.BoolVar(&all, "all", dAll, dAusage)
	flag.BoolVar(&all, "a", dAll, dAusage+" (shorthand)")
}

func getOwnership(info os.FileInfo) (uid, gid string) {
	// https://golang.org/pkg/syscall/#Stat_t
	stat := info.Sys().(*syscall.Stat_t)
	uid = strconv.FormatUint(uint64(stat.Uid), 10)
	gid = strconv.FormatUint(uint64(stat.Gid), 10)
	if u, err := user.LookupId(uid); err == nil {
		uid = u.Username
	} else {
		fmt.Println(err)
	}
	if g, err := user.LookupGroupId(gid); err == nil {
		gid = g.Name
	} else {
		fmt.Println(err)
	}
	return
}

func listItem(info os.FileInfo) {
	name := info.Name()
	if strings.HasPrefix(name, ".") && !all {
		return
	}
	if details {
		perm := info.Mode().String()
		mtime := info.ModTime().Format("Jan _2 15:04 2006")
		size := info.Size()
		uid, gid := getOwnership(info)
		str := "-%s %d\t%s %s\t%s\t%s\n"
		if info.IsDir() {
			str = "d%s %d\t%s %s\t%s\t%s\n"
		}
		fmt.Printf(str, perm, size, uid, gid, mtime, name)
	} else {
		fmt.Print(name + "  ")
	}
}

func readDir(path string) {
	filesInfo, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}

	for _, info := range filesInfo {
		listItem(info)
	}

	if !details {
		fmt.Println()
	}
}

func getWd() string {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}
	return pwd
}

func main() {
	flag.Parse()
	var paths []string

	// get the list of paths to be processed
	paths = flag.Args()
	if flag.NArg() == 0 {
		paths = append(paths, getWd())
	}

	for _, path := range paths {
		if info, err := os.Stat(path); err != nil {
			fmt.Println(err)
		} else if info.IsDir() {
			readDir(path)
		} else {
			listItem(info)
			if !details {
				fmt.Println()
			}
		}
	}
	os.Exit(0)
}
