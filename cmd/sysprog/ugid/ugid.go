// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 5

	This program illustrates `How to Retrieve File Ownership Information in
	Golang`

	https://fosstel.com/post/golang/file-ownership/
*/

package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"strconv"
	"syscall"
)

func getNames(uid, gid uint32) (string, string, error) {
	usr := strconv.FormatUint(uint64(uid), 10)
	group := strconv.FormatUint(uint64(gid), 10)
	if u, err := user.LookupId(usr); err == nil {
		usr = u.Username
	} else {
		return "", "", err
	}
	if g, err := user.LookupGroupId(group); err == nil {
		group = g.Name
	} else {
		return "", "", err
	}
	return usr, group, nil
}

func getOwnership(info os.FileInfo) (uid, gid uint32) {
	// https://golang.org/pkg/syscall/#Stat_t
	stat := info.Sys().(*syscall.Stat_t)
	return stat.Uid, stat.Gid
}

func main() {
	flag.Parse()
	for _, path := range flag.Args() {
		if info, err := os.Stat(path); err != nil {
			fmt.Println(err)
		} else {
			uid, gid := getOwnership(info)
			if usr, group, err := getNames(uid, gid); err != nil {
				fmt.Printf("user/group error: %s (%s, %s, %s)\n",
					err, path, usr, group)
			} else {
				fmt.Printf("%s: %s, %s (%d, %d)\n",
					path, usr, group, uid, gid)
			}
		}
	}
}
