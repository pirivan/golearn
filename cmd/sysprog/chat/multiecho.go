// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 12

	See the accompanying README file for details on this program.
*/

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
	"text/tabwriter"

	"gitlab.com/pirivan/golearn/internal/pkg/tcpserver"
)

const meVersion = "1.0"

type clientState struct {
	enable   bool
	mute     bool
	msgIn    int
	msgMuted int
}

// getCommand parses the /quit, /mute, and /unmute chat commands.
func getCommand(msg *tcpserver.ClientData) string {
	if msg.Data[0] != '/' || len(msg.Data) < 5 {
		return ""
	}
	cmd := string(msg.Data[1:5])
	if cmd == "quit" {
		return "quit"
	}
	if cmd == "mute" {
		return "mute"
	}
	if len(msg.Data) < 7 {
		return "other"
	}
	cmd = string(msg.Data[1:7])
	if cmd == "unmute" {
		return "unmute"
	}
	return "other"
}

// printStats prints the stats summary table before the program exits.
func printStats(clients map[string]*clientState) {
	if len(clients) == 0 {
		return
	}
	w := new(tabwriter.Writer)
	defer w.Flush()

	w.Init(os.Stdout, 0, 8, 1, '\t', 0) // format tab-separated columns with a tab stop of 8.
	fmt.Fprint(w, "\t\t\t\n")
	fmt.Fprint(w, "\tMessages\tMessages\t\n")
	fmt.Fprint(w, "Client ID\tEchoed\tMuted\tUsed /quit\n")
	for id, state := range clients {
		fmt.Fprintf(w, "%s\t%d\t%d\t%t\n", id, state.msgIn, state.msgMuted, !state.enable)
	}
}

// multiEcho implements the multi-echo functionality of the chat server.
func multiEcho(ctx context.Context, channels *tcpserver.AppChannels) {
	key := tcpserver.ServerCtx("wg")
	wg := ctx.Value(key).(*sync.WaitGroup)
	defer wg.Done()

	clients := make(map[string]*clientState)

	sendMsg := func(id string, data []byte) {
		msgOut := new(tcpserver.ClientData)
		msgOut.ID = id
		msgOut.Data = data
		channels.DataOut <- msgOut
	}

	echoMsg := func(msg *tcpserver.ClientData) {
		clients[msg.ID].msgIn++
		src := fmt.Sprintf("(%s) %s", msg.ID, string(msg.Data))
		for id, state := range clients {
			if id != msg.ID && state.enable {
				sendMsg(id, []byte(src))
			}
		}
	}

L:
	for {
		select {
		// app is shutting down
		case <-ctx.Done():
			log.Print("multiecho server is shutting down")
			printStats(clients)
			break L

		// incoming ctrl message from tcp server
		case ctrl := <-channels.CtrlIn:
			if ctrl.Enable {
				c := new(clientState)
				c.enable = true
				clients[ctrl.ID] = c
				src := fmt.Sprintf("multiecho server v%s\nclient id: %s\n", meVersion, ctrl.ID)
				sendMsg(ctrl.ID, []byte(src))
			} else {
				clients[ctrl.ID].enable = false
			}

		// incoming data from client
		case msg := <-channels.DataIn:
			switch getCommand(msg) {
			case "quit":
				ctrl := new(tcpserver.ClientCtrl)
				ctrl.ID = msg.ID
				ctrl.Enable = false
				channels.CtrlOut <- ctrl
			case "mute":
				clients[msg.ID].mute = true
			case "unmute":
				clients[msg.ID].mute = false
			default:
				if clients[msg.ID].mute {
					clients[msg.ID].msgMuted++
				} else if len(msg.Data) > 2 {
					echoMsg(msg)
				}
			}
		}
	}
}
