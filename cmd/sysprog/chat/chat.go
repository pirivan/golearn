// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 12

	See the accompanying README file for details on this program.
*/

package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"gitlab.com/pirivan/golearn/internal/pkg/tcpserver"
)

type jobType struct {
	address string
}

const version = "1.0"

var job jobType

// init() parses the command line and sets up the global `job` variable with the
// captured details.
func init() {
	portPtr := flag.String("p", "0", "TCP port number, default is random")

	flag.Parse()
	if len(flag.Args()) > 0 {
		log.Fatalf("unknown options \"%s\"", strings.Join(flag.Args(), " "))
	}

	if *portPtr == "0" {
		log.Fatal("invalid tcp port number 0")
	}
	job.address = ":" + *portPtr
}

// SIGTERM signal handler
func sigHandler(signals chan os.Signal, cancel context.CancelFunc) {
	<-signals
	log.Print("interrupt signal received")
	cancel()
}

// confLogging configures the chat logging service.
// Log entries are recorded on the local file `chat.log`
func confLogging() *os.File {
	f, err := os.OpenFile("chat.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening log file: %s", err)
	}
	log.SetOutput(f)
	log.Printf("Chat v%s", version)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	return f
}

func main() {
	// configure logging
	defer confLogging().Close()

	// app and comm wait groups
	var wg sync.WaitGroup
	var commWG sync.WaitGroup

	// app and comm contexts
	key := tcpserver.ServerCtx("wg")
	ctx0 := context.WithValue(context.Background(), key, &wg)
	ctx, cancel := context.WithCancel(ctx0)
	commCtx0 := context.WithValue(context.Background(), key, &commWG)
	commCtx, commCancel := context.WithCancel(commCtx0)

	// signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, os.Kill, syscall.SIGINT)
	go sigHandler(sigs, commCancel)

	server := new(tcpserver.TCPServer)
	server.Address = job.address
	channels, err := server.Init()
	if err != nil {
		log.Fatal(err)
	}

	// the multiecho app
	wg.Add(1)
	go multiEcho(ctx, channels)

	// the tcp server.
	// The signal handler triggers the server's shutdown sequence
	commWG.Add(1)
	go server.Start(commCtx)
	commWG.Wait() // tcp server is finished

	// close the multiecho app now
	cancel()
	wg.Wait()
}
