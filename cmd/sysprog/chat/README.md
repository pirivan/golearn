# A Multi-Echo Chat Server

Note: Go System Programming, Chapter 12

This program implements a chat server where any non-empty line written by a
client is echoed to all other clients. The program is structured into two source
files, `chat.go` which parses the command line and initializes all internal
structures, and `multiecho.go` that implements the message echoing functionality.

## The Chat Server Program

To start the chat server use the following command: 

```bash
bin/chat -p 9090
```

The `-p` option specifies the TCP port the chat server must listen on for client
connections.

### Stopping the Chat Server

The chat server runs until you enter `Ctrl-C` on the keyboard. More generally,
the chat server runs until it receives one of the `os.Interrupt`, `os.Kill`, or
`syscall.SIGINT` system signals.

Before closing, the chat server displays a table reporting for each client the
number of messages echoed, the number of messages sent while muted (see below),
and whether the client used the `/quit` command (see below) to exit or not.
Here is a sample summary table:

```text
                Messages        Messages
Client ID       Echoed          Muted           Used /quit
127.0.0.1:51012 5               0               true
127.0.0.1:51038 6               0               false
127.0.0.1:51046 2               3               false
```

### The Log File

The chat server logs messages to the `chat.log` file in the local directory
where the chat server starts. The logging facility appends new messages if the
log file exists already. Here is a sample log file:

```text
2018/08/15 12:55:33 Chat v1.0
2018/08/15 12:55:33 tcpserver.go:52: tcp server listening on address 0.0.0.0:9090
2018/08/15 12:55:37 tcpserver.go:108: new connection 127.0.0.1:56144
2018/08/15 12:55:41 tcpserver.go:108: new connection 127.0.0.1:56154
2018/08/15 12:55:42 tcpserver.go:108: new connection 127.0.0.1:56160
2018/08/15 13:00:17 connmgr.go:65: closing connection: 127.0.0.1:56144
2018/08/15 13:00:19 tcpserver.go:108: new connection 127.0.0.1:56762
2018/08/15 13:00:31 connmgr.go:65: closing connection: 127.0.0.1:56762
2018/08/15 13:08:07 chat.go:67: interrupt signal received
2018/08/15 13:08:07 tcpserver.go:147: tcp server is shutting down
2018/08/15 13:08:07 connmgr.go:65: closing connection: 127.0.0.1:56154
2018/08/15 13:08:07 connmgr.go:65: closing connection: 127.0.0.1:56160
2018/08/15 13:08:07 multiecho.go:93: multiecho server is shutting down
```

### The TCP Layer

The chat server uses the
[tcpserver](https://gitlab.com/pirivan/golearn/tree/master/internal/pkg/tcpserver)
library to manage the TCP connections. This library provides two control
channels and two data channels which are used as follows:

- The TCP layer uses the `ctrlIn` control channel to notify the chat server
  when new clients connect and when existing clients close their connection.

- The chat server uses the `ctrlOut` control channel to request the closing of a
  client connection. This is done in response to the client sending the `/quit`
  command.

- The TCP layer uses the `dataIn` data channel to push incoming client messages
  to the chat server. Each message on this channel uniquely identifies the
  source client by its IP:port address.

- The chat server uses the `dataOut` data channel to send messages to the
  clients. The multi-echo application uses this channel to send the echoed
  messages.

For further details on the `tcpserver` layer see the corresponding
documentation available from the link above.

## The Clients

The chat server has been tested using the `telnet` command as a client. Note
that `telnet` uses the two-character sequence `CR-LF` by default to mark the
end-of-line. The chat server uses this fact to avoid echoing empty lines.

## Echoing Messages

The following is a sample three-client session:

```text
$ telnet localhost 9090
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
multiecho server v1.0
client id: 127.0.0.1:53362
one
(127.0.0.1:53354) two
(127.0.0.1:53340) three

───────────────────────────────────────────────────
$ telnet localhost 9090
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
multiecho server v1.0
client id: 127.0.0.1:53354
(127.0.0.1:53362) one
two
(127.0.0.1:53340) three

───────────────────────────────────────────────────
$ telnet localhost 9090
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
multiecho server v1.0
client id: 127.0.0.1:53340
(127.0.0.1:53362) one
(127.0.0.1:53354) two
three
```

In this example, the client with id `127.0.0.1:53362` sends the message _one_,
then client `127.0.0.1:53354` sends the message _two_, and finally client
`127.0.0.1:53340` sends message the _three_. The chat server echoes these
messages prefixed with the id of the source client in parenthesis.

## Muting and Unmutting a Client

Clients can instruct the chat server not to echo their messages by sending the
`/mute` command. They can send the `/unmute` command to resume normal echoing.
The following is a sample session where client `127.0.0.1:56154` mutes its
session before sending the _I'm mute_ message.

```text
multiecho server v1.0
client id: 127.0.0.1:56160
one
(127.0.0.1:56154) two
(127.0.0.1:56144) three

───────────────────────────────────────────────────
multiecho server v1.0
client id: 127.0.0.1:56154
(127.0.0.1:56160) one
/mute
I'm mute
/unmute
two
(127.0.0.1:56144) three

───────────────────────────────────────────────────
multiecho server v1.0
client id: 127.0.0.1:56144
(127.0.0.1:56160) one
(127.0.0.1:56154) two
three
```

## Closing a Session

Clients can close their own session by sending the `/quit` command to the
chat server, as illustrated in the following example:

```text
multiecho server v1.0
client id: 127.0.0.1:56762
one
/quit
Connection closed by foreign host.
```
