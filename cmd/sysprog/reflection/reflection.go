// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 3, reflection.go

	The reflection program illustrates how to traverse a structure recursively
	using reflection to modify its fields. The program prints the content of a
	sample, hard-coded, structure before and after modification.
*/

package main

import (
	"fmt"
	"reflect"
	"strings"
)

// modifyObject takes an interface as input and recursively iterates over its
// fields until all of them are visited. In doing so, the function modifies each
// field to illustrate how this can be done through reflection. The input
// interface is expected to be a pointer to an object, typically a structure, so
// that the modifications can take place. The function exits when the input is
// not a pointer.
// modifyObject returns a multi-line string with lines describing its
// operations.
func modifyObject(obj interface{}) string {
	var objType = reflect.TypeOf(obj)
	var objValue = reflect.ValueOf(obj)

	// exit if the input is not a pointer to an object
	if objValue.Kind() != reflect.Ptr {
		return "Not a pointer\n"
	}

	// the output string
	var sout string

	// retrieve the object pointed to by the input
	elem := objValue.Elem()
	sout = fmt.Sprintf("%s of type %s, value %v\n", elem.Kind(), objType.Elem(), elem.Interface())
	switch elem.Kind() {
	case reflect.Array:
		sout += fmt.Sprintln("case array (TODO: how to modify the original array)")
		// the following code modifies a copy of the original array
		//arr := elem.Interface().([3]int)
		//arr[0] = 0
		//fmt.Println(arr)
	case reflect.Bool:
		elem.SetBool(!elem.Bool())
		sout += fmt.Sprintf("case boolean %t\n", elem.Bool())
	case reflect.Float64:
		elem.SetFloat(elem.Float() + 1.0)
		sout += fmt.Sprintf("case float64 %.2f\n", elem.Float())
	case reflect.Func:
		sout += fmt.Sprintln("case function (invoke the function)")
		//f := elem.Interface().(func(string) string)
		//sout += fmt.Sprintf("%s\n", f("function called"))
		sout += fmt.Sprintf("%s\n", elem.Call([]reflect.Value{reflect.ValueOf("function called")})[0])
	case reflect.Int:
		elem.SetInt(elem.Int() + 1)
		sout += fmt.Sprintf("case int %d\n", elem.Int())
	case reflect.Map:
		sout += fmt.Sprintln("case map (TODO: how to modify the original map)")
	case reflect.Slice:
		sout += fmt.Sprintln("case slice (TODO: how to modify the original slice)")
	case reflect.String:
		elem.SetString(elem.String() + " modified")
		sout += fmt.Sprintf("case string '%s'\n", elem.String())
	case reflect.Struct:
		sout += fmt.Sprintln("case structure")
		for i := 0; i < elem.NumField(); i++ {
			item := elem.Field(i).Addr().Interface()
			sout += modifyObject(item)
		}
	}
	return sout
}

// the level of recursion when iterating struct objects in traverseObject()
var level = -1

// traverseObject iterates over a structure object recursively listing its
// field's types and values. It returns a multi-line string with the results.
func traverseObject(obj interface{}) string {
	var objType = reflect.TypeOf(obj)
	var objValue = reflect.ValueOf(obj)

	// fetch the pointed to element if the input parameter is a pointer
	elem := objValue
	if objValue.Kind() == reflect.Ptr {
		elem = objValue.Elem()
		objType = reflect.TypeOf(elem)
		objValue = reflect.ValueOf(elem)
	}

	// the output string
	var sout string

	if elem.Kind() == reflect.Struct {
		rec := level
		if rec < 0 {
			rec = 0
		}
		sout = fmt.Sprintf("%s(struct) type %s, value %v\n", strings.Repeat("*", rec), objType, elem.Interface())
		level++
		for i := 0; i < elem.NumField(); i++ {
			item := elem.Field(i).Interface()
			sout += traverseObject(item)
		}
		level--
	} else {
		sout = fmt.Sprintf("%stype %s, value %v\n", strings.Repeat("*", level), objType, elem.Interface())
	}
	return sout
}

func main() {
	type t1 int
	type s1 struct {
		AnInt   int
		AString string
	}
	type Item struct {
		Name   string `json:"name"`
		Date   string `json:"date"`
		Qty    int    `json:"qty"`
		Dummy1 s1
		Dummy  t1 `key:"value"`
	}

	Objs := struct {
		AnArray    [3]int
		ABoolean   bool
		AFloat64   float64
		AFunction  func(string) string
		AnInt      int
		AStructure Item
		AMap       map[int]string
		ASlice     []int
		AString    string
	}{
		[3]int{1, 2, 3},
		false,
		3.14,
		func(s string) string { return (s + " modified") },
		1,
		Item{"Pedro", "2018-06-08", 10, s1{0, "blah"}, 0},
		make(map[int]string),
		[]int{1, 2, 3},
		"Hello",
	}

	fmt.Printf("Traversing the struct\n%s\n", traverseObject(Objs))
	fmt.Printf("Traversing the struct\n%s\n", traverseObject(&Objs))
	original := *&Objs
	fmt.Printf("Modifying the struct\n%s\n", modifyObject(Objs))
	fmt.Printf("Modifying the struct\n%s\n", modifyObject(&Objs))
	fmt.Println(original)
	fmt.Println(Objs)
}
