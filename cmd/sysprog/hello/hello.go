// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	https://golang.org/doc/code.html
*/

package main

import (
	"fmt"

	"gitlab.com/pirivan/golearn/internal/pkg/stringutils"
)

func main() {
	fmt.Printf(stringutils.Reverse("\n!dlroW olleH"))
}
