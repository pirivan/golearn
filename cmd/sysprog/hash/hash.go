// This is free and unencumbered software released into the public domain.
// See the LICENSE file for details.

/*
	Go System Programming, Chapter 4, hash.go

	This program creates a sample hash table while guaranteeing that there are
	no duplicate entries.
*/

package main

import (
	"fmt"
)

// Node is a node in the hash table
type Node struct {
	Value int
	Next  *Node
}

// HashTable is a hash table of fixed size
type HashTable struct {
	Table map[int]*Node
	Size  int
}

func hashFunction(i, size int) int {
	return (i % size)
}

func search(hash *HashTable, index int, value int) bool {
	r := false
	t := hash.Table[index]
	for t != nil {
		if t.Value == value {
			r = true
			break
		}
		t = t.Next
	}
	return r
}

func insert(hash *HashTable, value int) int {
	index := hashFunction(value, hash.Size)
	if search(hash, index, value) {
		return -1
	}
	element := Node{Value: value, Next: hash.Table[index]}
	hash.Table[index] = &element
	return index
}

func traverse(hash *HashTable) {
	for k := range hash.Table {
		if hash.Table[k] != nil {
			t := hash.Table[k]
			for t != nil {
				fmt.Printf("%d -> ", t.Value)
				t = t.Next
			}
			fmt.Println()
		}
	}
}

func main() {
	table := make(map[int]*Node, 10)
	hash := &HashTable{Table: table, Size: 10}
	fmt.Println("Number of spaces:", hash.Size)
	for i := 0; i < 95; i++ {
		insert(hash, i)
	}
	for i := 0; i < 95; i++ {
		if insert(hash, i) != -1 {
			fmt.Print("x")
		}
	}
	traverse(hash)
}
